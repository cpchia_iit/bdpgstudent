import sys
from kafka import KafkaConsumer

if __name__ == "__main__":
  if len(sys.argv) != 2:
    print("Usage: consumer.py <topic>")
    exit(-1)

# To consume latest messages and auto-commit offsets
consumer = KafkaConsumer(sys.argv[1])
print("Listening to message queue " + sys.argv[1])

for message in consumer:
    print (message.value)
