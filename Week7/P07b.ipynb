{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Practical 7b - Structured Streaming\n",
    "In this exercise, we are going to be working with a sample of the Heterogeneity Human Activity Recognition dataset. The data consists of smartphone and smartwatch sensor readings from a variety of devices - specifically, the accelerometer and gyroscope, sampled at the highest possible frequency supported by the devices. Readings from these sensors were recorded while users performed activities like biking, sitting, standing, walking, and so on. There are several different smartphones and smartwatches used, and nine total users. The data has been downloaded for you and they are stored in multiple files (in json format) located in the `data/activity-data` directory. There are a total of ten fields for each record in the data files and their descriptions are as follows:\n",
    "- Index: An identifier for the record\n",
    "- Device: The device used for recording the activity. There are only two devices in this sample dataset - nexus4_1 and nexus4_2 \n",
    "- Model: Model of the device. There is only one model in this sample dataset - nexus4.\n",
    "- User: The identifier for the user (a to i)\n",
    "- Creation_Time: Specifies when an event was created.\n",
    "- Arrival_Time: Specifies when an event hits the servers somewhere upstream.\n",
    "- x, y and z: The six axes from the accelerometer is captured in the x,y and z fields.\n",
    "- gt: Specifies what activity the user was doing at that time. Possible values - stairsup, sit, stand, walk, bike, stairsdown. Can also be `null`.\n",
    "\n",
    "The following shows a sample of 5 lines from the dataset:\n",
    "```\n",
    "{\"Arrival_Time\":1424686735175,\"Creation_Time\":1424686733176178965,\"Device\":\"nexus4_1\",\"Index\":35,\"Model\":\"nexus4\",\"User\":\"g\",\"gt\":\"stand\",\"x\":0.0014038086,\"y\":5.0354E-4,\"z\":-0.0124053955}\n",
    "{\"Arrival_Time\":1424686735378,\"Creation_Time\":1424686733382813486,\"Device\":\"nexus4_1\",\"Index\":76,\"Model\":\"nexus4\",\"User\":\"g\",\"gt\":\"stand\",\"x\":-0.0039367676,\"y\":0.026138306,\"z\":-0.01133728}\n",
    "{\"Arrival_Time\":1424686735577,\"Creation_Time\":1424686733579072031,\"Device\":\"nexus4_1\",\"Index\":115,\"Model\":\"nexus4\",\"User\":\"g\",\"gt\":\"stand\",\"x\":0.003540039,\"y\":-0.034744263,\"z\":-0.019882202}\n",
    "{\"Arrival_Time\":1424686735779,\"Creation_Time\":1424688581834321412,\"Device\":\"nexus4_2\",\"Index\":163,\"Model\":\"nexus4\",\"User\":\"g\",\"gt\":\"stand\",\"x\":0.002822876,\"y\":0.005584717,\"z\":0.017318726}\n",
    "{\"Arrival_Time\":1424686735982,\"Creation_Time\":1424688582035859498,\"Device\":\"nexus4_2\",\"Index\":203,\"Model\":\"nexus4\",\"User\":\"g\",\"gt\":\"stand\",\"x\":0.0017547607,\"y\":-0.018981934,\"z\":-0.022201538}\n",
    "```\n",
    "\n",
    "First, we shall create a schema for the dataset:\n",
    "```python\n",
    "from pyspark.sql.types import *\n",
    "\n",
    "schema = StructType([\n",
    "    StructField(\"Index\", LongType(), True),\n",
    "    StructField(\"Device\", StringType(), True),\n",
    "    StructField(\"Model\", StringType(), True),\n",
    "    StructField(\"User\", StringType(), True),\n",
    "    StructField(\"Creation_Time\", LongType(), True),\n",
    "    StructField(\"Arrival_Time\", LongType(), True),\n",
    "    StructField(\"x\", DoubleType(), True),\n",
    "    StructField(\"y\", DoubleType(), True),\n",
    "    StructField(\"z\", DoubleType(), True),\n",
    "    StructField(\"gt\", StringType(), True)\n",
    "])\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We shall now create a streaming DataFrame which will read each input file in the directory one by one as if it is a stream. We will set the option `maxFilesPerTrigger` to control how quickly Spark will read all of the files in the directory. By setting this value to 1, we are artificially limiting the flow of the stream to one file per trigger to simulate a real streaming scenario. However, this probably is not something you will use in production.\n",
    "```python\n",
    "streaming = spark.readStream.schema(schema).option(\"maxFilesPerTrigger\", 1) \\\n",
    "                                           .json(\"/home/training/data/activity-data\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step in our analysis is to convert the timestamp column into the proper Spark SQL timestamp type. Our current `Creation_Time` column is unixtime nanoseconds (represented as a long). Therefore, we are going to have to do a little manipulation (convert to seconds then to timestamp) to get it into the proper format:\n",
    "```python\n",
    "withEventTime = streaming.withColumn(\"event_time\", (streaming.Creation_Time.cast(\"double\") / 1000000000) \\\n",
    "                                     .cast(\"timestamp\"))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us count the number of occurrences of an event in 10-minute windows without any overlap in timing between windows. For example, we can have one window as [12:00 to 12:10) and another as [12:10 to 12:20) but cannot have a window that is [12:05 to 12:15). As new events are being added to our system in real time, Structured Streaming would update those counts accordingly.\n",
    "```python\n",
    "import pyspark.sql.functions as fn\n",
    "\n",
    "query = withEventTime.groupBy(fn.window(fn.col(\"event_time\"), \"10 minutes\")).count() \\\n",
    "    .writeStream.queryName(\"events_per_window\").format(\"memory\").outputMode(\"complete\") \\\n",
    "    .trigger(processingTime='5 seconds').start()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can view the running counts of the events from the memory table `events_per_window` by executing the following SQL query every few seconds:\n",
    "```python\n",
    "spark.sql(\"SELECT * FROM events_per_window\").show(truncate=False)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, you can stop the query using:\n",
    "```python\n",
    "query.stop()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
