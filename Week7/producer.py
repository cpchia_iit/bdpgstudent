import sys
from kafka import KafkaProducer

if __name__ == "__main__":
  if len(sys.argv) != 3:
    print("Usage: producer.py <topic> <message>")
    exit(-1)

producer = KafkaProducer(bootstrap_servers='localhost:9092')
future = producer.send(sys.argv[1], bytes(sys.argv[2], 'utf-8'))

record_metadata = future.get(timeout=10)
print(record_metadata.topic)
print(record_metadata.offset)
producer.close()

