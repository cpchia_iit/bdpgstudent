# adapted from http://www.awesomestats.in/spark-twitter-stream/

import tweepy
import socket
import json
import sys

consumer_key = "ENTER YOUR CONSUMER KEY"
consumer_secret = "ENTER YOUR CONSUMER SECRET"
access_token = "ENTER YOUR ACCESS TOKEN"
access_token_secret = "ENTER YOUR ACCESS TOKEN SECRET"

class TweetsListener(tweepy.StreamListener):
  def __init__(self, csocket):
      self.client_socket = csocket

  def on_data(self, data):
      try:
          msg = json.loads(data)
          print(msg['text'].encode('utf-8'))
          self.client_socket.send(msg['text'].encode('utf-8'))
          return True
      except BaseException as e:
          print("Error on_data: %s" % str(e))
      return True

  def on_status(self, status):
    print(status.text)

  def on_error(self, status_code):
    if status_code == 420:
      # returning False in on_data disconnects the stream
      return False
    print(status_code)
    return True

def sendData(c_socket, word):
  auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
  auth.set_access_token(access_token, access_token_secret)

  twitter_stream = tweepy.Stream(auth, TweetsListener(c_socket))
  twitter_stream.filter(track=word)

if __name__ == "__main__":
  if len(sys.argv) == 1:
    print("Usage: tweetreader.py <search_term(s)>")
    exit(-1)

  s = socket.socket()         # Create a socket object
  host = "127.0.0.1"          # Get local machine name
  port = 5555                 # Reserve a port for your service.
  s.bind((host, port))        # Bind to the port

  print("Listening on port: %s" % str(port))

  s.listen(5)                 # Now wait for client connection.
  c, addr = s.accept()        # Establish connection with client.

  print("Received request from: " + str(addr))

  sendData(c, sys.argv[1:])
