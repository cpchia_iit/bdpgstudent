{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Practical 5b - Natural Language Processing (Solution)\n",
    "In this exercise, we shall take a look at using Spark for Natural Language Processing (NLP). NLP helps analyze raw textual data and extract useful information such as sentence structure, sentiment of text, or even translation of text between languages. Following from the previous exercise, we shall apply NLP to analyze the sentiment of movie reviews. Based on annotated training data, we will build a classification model that is going to distinguish between positive and negative movie reviews.\n",
    "\n",
    "We shall be using the Large Movie Review Database from http://ai.stanford.edu/~amaas/data/sentiment/. A copy of of the dataset is already in your `data/aclImdb` directory. There are two subfolders labeled `train` and `test`. In the `train` folder, there are 12,500 positive reviews and 12,500 negative reviews that we will build a classifier on. The `test` folder contains the same amount of positive and negative reviews for a grand total of 50,000 positive and negative reviews amongst the two datasets.\n",
    "\n",
    "Let us look at an example of one review to see what the data looks like:\n",
    "\n",
    "_\"Bromwell High is nothing short of brilliant. Expertly scripted and perfectly delivered, this searing parody of  students and teachers at a South London Public School leaves you literally rolling with laughter. It's vulgar, provocative, witty and sharp. The characters are a superbly caricatured cross-section of British society (or to be more accurate, of any society). Following the escapades of Keisha, Latrina, and Natella, our three \"protagonists\", for want of a better term, the show doesn't shy away from parodying every imaginable subject. Political correctness flies out the window in every episode. If you enjoy shows that aren't afraid to poke fun of every taboo subject imaginable, then Bromwell High will not disappoint!\"_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data Preparation\n",
    "The first step is to read in the training dataset, which is composed of 25,000 positive and negative movie reviews. At the same time, we need to create our binary labels of 0 for a negative review and 1 for a positive review. \n",
    "\n",
    "Let us start with reading all the positive and negative reviews first:\n",
    "```python\n",
    "pos_reviews = spark.read.text(\"/home/training/data/aclImdb/train/pos/*.txt\")\n",
    "print(f\"Number of positive reviews: {pos_reviews.count()}\")\n",
    "neg_reviews = spark.read.text(\"/home/training/data/aclImdb/train/neg/*.txt\")\n",
    "print(f\"Number of negative reviews: {neg_reviews.count()}\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Number of positive reviews: 12500\n",
      "Number of negative reviews: 12500\n"
     ]
    }
   ],
   "source": [
    "pos_reviews = spark.read.text(\"/home/training/data/aclImdb/train/pos/*.txt\")\n",
    "print(f\"Number of positive reviews: {pos_reviews.count()}\")\n",
    "neg_reviews = spark.read.text(\"/home/training/data/aclImdb/train/neg/*.txt\")\n",
    "print(f\"Number of negative reviews: {neg_reviews.count()}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each row of each DataFrame contains a string representing a single review. We shall generate the corresponding labels and merge both datasets together. The labeling is easy since we loaded negative and positive reviews as separate DataFrames. We can directly append a constant column representing the label 0 for negative reviews and 1 for positive reviews:\n",
    "```python\n",
    "import pyspark.sql.functions as fn\n",
    "\n",
    "pos = pos_reviews.withColumn(\"label\", fn.lit(1.0))\n",
    "neg = neg_reviews.withColumn(\"label\", fn.lit(0.0))\n",
    "movie_reviews = pos.union(neg).withColumn(\"row_id\", fn.monotonically_increasing_id())\n",
    "movie_reviews.show(5)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+--------------------+-----+------+\n",
      "|               value|label|row_id|\n",
      "+--------------------+-----+------+\n",
      "|Match 1: Tag Team...|  1.0|     0|\n",
      "|**Attention Spoil...|  1.0|     1|\n",
      "|Titanic directed ...|  1.0|     2|\n",
      "|By now you've pro...|  1.0|     3|\n",
      "|*!!- SPOILERS - !...|  1.0|     4|\n",
      "+--------------------+-----+------+\n",
      "only showing top 5 rows\n",
      "\n"
     ]
    }
   ],
   "source": [
    "import pyspark.sql.functions as fn\n",
    "\n",
    "pos = pos_reviews.withColumn(\"label\", fn.lit(1.0))\n",
    "neg = neg_reviews.withColumn(\"label\", fn.lit(0.0))\n",
    "movie_reviews = pos.union(neg).withColumn(\"row_id\", fn.monotonically_increasing_id())\n",
    "movie_reviews.show(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tokenization\n",
    "At this stage, we have only raw text representing reviews which is not sufficient to run any machine learning algorithm.  We need to extract relevant features from the text to build our model. We shall start with tokenization. A tokenizer converts the input string into lowercase and then splits the string with whitespaces into individual tokens. We can split a given sentence into words either using the default space delimiter or using a customised regular expression based tokenizer. In either case, the input column (our review text) is transformed into an output column (a list of words). For this exercise, we shall use the more powerful `RegexTokenizer`.\n",
    "```python\n",
    "import pyspark.ml.feature as ft\n",
    "\n",
    "MIN_TOKEN_LENGTH = 3\n",
    "tokenizer = ft.RegexTokenizer(inputCol='value', outputCol=\"RegexWords\", pattern='\\W+', minTokenLength=MIN_TOKEN_LENGTH)\n",
    "```\n",
    "The `RegexTokenizer` will by default change all the text into lowercase. This is fine in most cases. However, it is important that you are aware of the implications to your model when converting all text data to lowercase. As an example, \"Python,\" the computing language would lowercase to \"python,\" which is also a snake. Clearly, the two tokens are not the same; however, making both lowercase would make them so. If you do not want the `RegexTokenizer` to change all the text to lowercase, set `toLowercase=False`.\n",
    "\n",
    "Our tokenizer here will split the movie review text into tokens that are represented by alphanumeric characters only (`pattern='\\W+'`) and we only keep those tokens whose length is greater than three characters (`minTokenLength=MIN_TOKEN_LENGTH`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.feature as ft\n",
    "\n",
    "MIN_TOKEN_LENGTH = 3\n",
    "tokenizer = ft.RegexTokenizer(inputCol='value', outputCol=\"RegexWords\", pattern='\\W+', minTokenLength=MIN_TOKEN_LENGTH)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Removing Stop Words\n",
    "Stop words are essentially injunctions and conjunctions such as in, at, the, and, etc, that add no contextual meaning to the review that we want to classify and hence, should be removed. Without this cleansing process, the subsequent algorithms might be biased because of the common words. Spark provides a list of generic English stop words but you can override or extend the set of stop words to suit the purpose of your use case. Let us extend our list of stop words and then use the `StopWordRemover` transformer to remove the stop words.\n",
    "```python\n",
    "stop_words = ft.StopWordsRemover.loadDefaultStopWords(\"english\") + [\"ax\", \"arent\", \"re\"]\n",
    "remover = ft.StopWordsRemover(inputCol='RegexWords', outputCol='FilteredWords', stopWords=stop_words)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "stop_words = ft.StopWordsRemover.loadDefaultStopWords(\"english\") + [\"ax\", \"arent\", \"re\"]\n",
    "remover = ft.StopWordsRemover(inputCol='RegexWords', outputCol='FilteredWords', stopWords=stop_words)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating Word Combinations\n",
    "Tokenizing our text and filtering stop words leave us with a clean set of words to use as features. It is often of interest to look at combination of words, usually by looking at colocated words. Word combinations are technically referred to as _n-grams_. N-grams are sequences of words of length N. N-grams of length one are called unigrams, those of length two are called bigrams, and those of length three are called trigrams. Anything above those are just four-gram, five-gram, etc. Order matters with n-gram creation, so converting three words into bigram representations would result in two bigrams. The goal when creating n-grams is to try to capture sentence structure with more information than can be gleaned simply by looking at all words in a paragraph or sentence. We shall now create bigrams from our words using the `NGrams` transformer.\n",
    "```python\n",
    "bigram = ft.NGram(n=2, inputCol=\"FilteredWords\", outputCol=\"bigrams\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "bigram = ft.NGram(n=2, inputCol=\"FilteredWords\", outputCol=\"bigrams\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Feature Extraction\n",
    "We shall now work on converting our tokenized reviews into vectors, which can then be input to our machine learning algorithm. A common method is to use **TF-IDF**, or term frequency-inverse document frequency to represent each word. TF-IDF measures how important a word is to a document in a collection of documents. It is used extensively in informational retrieval and reflects the weightage of the word in the document. Words that occur in a few documents are given more weight than words that occur in many documents. In practice, a word like “the” would be weighted very low because of its prevalence while a word like “streaming” would occur in fewer documents and thus would be weighted higher.\n",
    "\n",
    "To compute the TF-IDF, we start with computing the _Term Frequency_ (TF) of each word. TF of a word measures the number of times the word occurs in a movie review. Spark provides the transformer `HashingTF` which takes a set of words and converts them into vectors of fixed length by hashing each word using a hash function to generate an index for each word. Then, word frequencies are generated using the indices of the hash table. Let us create this transformer now:\n",
    "```python\n",
    "raw_features = ft.HashingTF(inputCol=\"bigrams\", outputCol=\"RawFeatures\")\n",
    "```\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "raw_features = ft.HashingTF(inputCol=\"bigrams\", outputCol=\"RawFeatures\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we shall compute the TF-IDF values using the estimator `IDF` which works on the output generated by the `HashingTF` transformer to generate the TF-IDF values.\n",
    "```python\n",
    "idf = ft.IDF(inputCol=\"RawFeatures\", outputCol=\"features\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "idf = ft.IDF(inputCol=\"RawFeatures\", outputCol=\"features\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, create a pipeline and used it to generate a DataFrame that we can used for machine learning.\n",
    "```python\n",
    "from pyspark.ml import Pipeline\n",
    "pipeline = Pipeline( stages = [tokenizer, remover, bigram, raw_features, idf] )\n",
    "model = pipeline.fit(movie_reviews)\n",
    "features_df = model.transform(movie_reviews)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml import Pipeline\n",
    "pipeline = Pipeline( stages = [tokenizer, remover, bigram, raw_features, idf] )\n",
    "model = pipeline.fit(movie_reviews)\n",
    "features_df = model.transform(movie_reviews)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### Building a Naive Bayes Model\n",
    "Naive Bayes classifiers are a family of probabilistic classifiers based on applying the Bayes' conditional probability theorem. These classifiers assume independence between the features. Naive Bayes is often the baseline method for text categorization with word frequencies as the feature set. Despite the strong independence assumptions, the Naive Bayes classifiers are fast and easy to implement. Hence, they are used very commonly in practice. We shall build a Naive Bayes model for this exercise:\n",
    "```python\n",
    "import pyspark.ml.classification as cl\n",
    "nb = cl.NaiveBayes()\n",
    "nb_model = nb.fit(features_df)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.classification as cl\n",
    "nb = cl.NaiveBayes()\n",
    "nb_model = nb.fit(features_df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preparing the Test Data\n",
    "We shall now prepare our test data for testing our Naive Bayes model:\n",
    "```python\n",
    "test_pos_reviews = spark.read.text(\"/home/training/data/aclImdb/test/pos/*.txt\")\n",
    "test_neg_reviews = spark.read.text(\"/home/training/data/aclImdb/test/neg/*.txt\")\n",
    "test_pos = test_pos_reviews.withColumn(\"label\", fn.lit(1.0))\n",
    "test_neg = test_neg_reviews.withColumn(\"label\", fn.lit(0.0))\n",
    "test_reviews = test_pos.union(test_neg).withColumn(\"row_id\", fn.monotonically_increasing_id())\n",
    "test_features = pipeline.fit(test_reviews).transform(test_reviews)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_pos_reviews = spark.read.text(\"/home/training/data/aclImdb/test/pos/*.txt\")\n",
    "test_neg_reviews = spark.read.text(\"/home/training/data/aclImdb/test/neg/*.txt\")\n",
    "test_pos = test_pos_reviews.withColumn(\"label\", fn.lit(1.0))\n",
    "test_neg = test_neg_reviews.withColumn(\"label\", fn.lit(0.0))\n",
    "test_reviews = test_pos.union(test_neg).withColumn(\"row_id\", fn.monotonically_increasing_id())\n",
    "test_features = pipeline.fit(test_reviews).transform(test_reviews)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluating the Performance of the Model\n",
    "To evaluate the performance of our model, we can use the `BinaryClassificationEvaluator` class and its `evaluate` method:\n",
    "```python\n",
    "predictions = nb_model.transform(test_features)\n",
    "\n",
    "import pyspark.ml.evaluation as ev\n",
    "evaluator = ev.BinaryClassificationEvaluator(rawPredictionCol=\"probability\")\n",
    "print(evaluator.evaluate(predictions, {evaluator.metricName: 'areaUnderROC'}))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.7770164576000008\n"
     ]
    }
   ],
   "source": [
    "predictions = nb_model.transform(test_features)\n",
    "\n",
    "import pyspark.ml.evaluation as ev\n",
    "evaluator = ev.BinaryClassificationEvaluator(rawPredictionCol=\"probability\")\n",
    "print(evaluator.evaluate(predictions, {evaluator.metricName: 'areaUnderROC'}))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
