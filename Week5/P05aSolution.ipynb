{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Practical 5a - Building Recommendation Engines (Solution)\n",
    "A recommendation engine is a model that can predict what a user may be interested in. In this day and age, the need to build scalable real-time recommendations is increasing day by day. With more Internet users using e-commerce sites for their purchases, these e-commerce sites have realized the potential of understanding the patterns of the users' purchase behavior to improve their business, and to serve their customers on a very personalized level. With Spark, you can build a system which caters to a huge user base and generates recommendations in real time and in this exercise, we shall apply this to the context of movies and build a movie-recommendation engine. \n",
    "\n",
    "We shall use the MovieLens 100K dataset to build a collaborative filtering recommendation engine. The dataset contains 943 user ratings on 1,682 movies and the filename is called `udata.csv` which is located in the `data` directory. The columns, each separated by a **tab**, are as follows:\n",
    "\n",
    "- UserID: the id of the user\n",
    "- ItemID: the id of the movie\n",
    "- Rating: the rating given by the user\n",
    "- Timestamp: the timestamp of this record. The timestamps are unix seconds since 1/1/1970 UTC.\n",
    "\n",
    "There is no header line and the first 10 lines are as follows:\n",
    "```\n",
    "196 242 3 881250949\n",
    "186 302 3 891717742\n",
    "22  377 1 878887116\n",
    "244 51  2 880606923\n",
    "166 346 1 886397596\n",
    "298 474 4 884182806\n",
    "115 265 2 881171488\n",
    "253 465 5 891628467\n",
    "305 451 3 886324817\n",
    "6   86  3 883603013\n",
    "```\n",
    "### Loading the Data\n",
    "First, let us create the schema and load the data into a DataFrame:\n",
    "```python\n",
    "from pyspark.sql.types import *\n",
    "\n",
    "schema = StructType( [\n",
    "    StructField(\"UserID\", IntegerType(), True),\n",
    "    StructField(\"ItemID\", IntegerType(), True),\n",
    "    StructField(\"Rating\", DoubleType(), True),\n",
    "    StructField(\"Timestamp\", StringType(), True),\n",
    "])\n",
    "movies_df = spark.read.csv(\"/home/training/data/udata.csv\", schema=schema, sep='\\t')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.sql.types import *\n",
    "\n",
    "schema = StructType( [\n",
    "    StructField(\"UserID\", IntegerType(), True),\n",
    "    StructField(\"ItemID\", IntegerType(), True),\n",
    "    StructField(\"Rating\", DoubleType(), True),\n",
    "    StructField(\"Timestamp\", StringType(), True),\n",
    "])\n",
    "movies_df = spark.read.csv(\"/home/training/data/udata.csv\", schema=schema, sep='\\t')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Building the Recommendation Engine\n",
    "We first start by dividing the original data into training and test datasets randomly using the `randomSplit` method:\n",
    "```python\n",
    "(train_data, test_data) = movies_df.randomSplit([0.8, 0.2])\n",
    "train_data.persist()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "DataFrame[UserID: int, ItemID: int, Rating: double, Timestamp: string]"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(train_data, test_data) = movies_df.randomSplit([0.8, 0.2], seed=12345)\n",
    "train_data.persist()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Spark, there is only one recommendation workhorse algorithm, the **Alternating Least Squares** (ALS) algorithm. This algorithm leverages ratings given by other users to make recommendations to users that may share similar ratings. It does this by finding the latent factors that describe the users and the movies and alternating between predicting one, given the inputs of the other. Therefore, this algorithm requires three inputs. A user column, an item column (like a movie), and a rating column (which either is an implicit behavior or explicit rating). The user and item columns must be integers as opposed to doubles. The ALS algorithm in Spark can scale extremely well to millions of users, millions of items, and billions of ratings.\n",
    "\n",
    "We shall now load the ALS module into the Spark environment, call the `train` method to train the model and pass the required parameters such as rank, number of iterations (`maxIter`), and training data to the `train` method.\n",
    "```python\n",
    "from pyspark.ml.recommendation import ALS\n",
    "\n",
    "als = ALS(userCol=\"UserID\", itemCol=\"ItemID\", ratingCol=\"Rating\", coldStartStrategy=\"drop\", regParam=0.05)\n",
    "als_model = als.fit(train_data)\n",
    "predictions = als_model.transform(test_data)\n",
    "```\n",
    "These are some of the common parameters you can set:\n",
    "- `rank`: This refers to the number of factors in our ALS model i.e. the number of hidden features. Generally, the greater the number of factors, the better, but this has a direct impact on memory usage, both for computation and to store models for serving, particularly for large numbers of users or items. Hence, this is often a trade-off in real-world use cases. It also impacts the amount of training data required. A rank in the range of 10 to 200 is usually reasonable. The default is 10.\n",
    "- `maxIter`: This refers to the number of iterations to run. ALS models will converge to a reasonably good solution after relatively little iterations. So, we do not need to run too many iterations in most cases, around 10 is often a good default.\n",
    "- `regParam`: This specifies the regularization parameter in ALS (defaults to 1.0). This parameter is important for numerical stability, and some kind of regularization is almost always used.\n",
    "- `coldStartStrategy`: This is for handling user/item ids the model has not seen in the training data when used for prediction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml.recommendation import ALS\n",
    "\n",
    "als = ALS(userCol=\"UserID\", itemCol=\"ItemID\", ratingCol=\"Rating\", coldStartStrategy=\"drop\", regParam=0.05)\n",
    "als_model = als.fit(train_data)\n",
    "predictions = als_model.transform(test_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluation\n",
    "The proper way to evaulate ALS in the context of Spark is to use the `RegressionEvaluator`. Just like with a conventional regression, we are trying to predict a real value. In the ALS case, this is a rating or preference level.\n",
    "```python\n",
    "import pyspark.ml.evaluation as ev\n",
    "\n",
    "evaluator = ev.RegressionEvaluator(predictionCol=\"prediction\", labelCol=\"Rating\")\n",
    "print(evaluator.evaluate(predictions, {evaluator.metricName: \"rmse\"}))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.9578850318671208\n"
     ]
    }
   ],
   "source": [
    "import pyspark.ml.evaluation as ev\n",
    "\n",
    "evaluator = ev.RegressionEvaluator(predictionCol=\"prediction\", labelCol=\"Rating\")\n",
    "print(evaluator.evaluate(predictions, {evaluator.metricName: \"rmse\"}))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
