{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## E-Learning 2a - Using Spark SQL for Data Processing (Solution)\n",
    "Apache Spark is an open-source powerful distributed querying and processing engine. From this point on, we will be writing Python programs that will be accessing the Spark APIs so that we can process big data efficiently. We will be writing our Spark programs using the Jupyter Notebook so that we can perform our analytics interactively.\n",
    "\n",
    "In this first exercise, you will learn to how write your programs using Spark SQL, one of the most advanced components of Apache Spark. When you type `pyspark` at the command line, the Spark engine automatically creates a `SparkSession` object for you called `spark`. This object represents a unified entry point for manipulating data in Spark. Let us first make sure that this object exists by typing `spark` in the cell below.\n",
    "```python\n",
    "spark\n",
    "```\n",
    "Note that depending on the speed of your notebook, it might take a few seconds before any output appears. You should see that the version of the Spark engine is v2.3.2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "\n",
       "            <div>\n",
       "                <p><b>SparkSession - hive</b></p>\n",
       "                \n",
       "        <div>\n",
       "            <p><b>SparkContext</b></p>\n",
       "\n",
       "            <p><a href=\"http://10.0.2.15:4040\">Spark UI</a></p>\n",
       "\n",
       "            <dl>\n",
       "              <dt>Version</dt>\n",
       "                <dd><code>v2.3.2</code></dd>\n",
       "              <dt>Master</dt>\n",
       "                <dd><code>local[*]</code></dd>\n",
       "              <dt>AppName</dt>\n",
       "                <dd><code>PySparkShell</code></dd>\n",
       "            </dl>\n",
       "        </div>\n",
       "        \n",
       "            </div>\n",
       "        "
      ],
      "text/plain": [
       "<pyspark.sql.session.SparkSession at 0x7fb033dfdfd0>"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "spark"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To query and process structured data using Spark SQL, we need to impose a structure called **DataFrame** onto a distributed collection of data. The DataFrame is an immutable distributed collection of data that is organised into named columns analogous to a table in a relational database. Before we create our first DataFrame, let us look at the dataset we will be using. The data file is `job-vacancy-by-industry.csv` and is located in the `/home/training/data` directory. It contains information of job vacancies for various industries (including sub-industries) from 1990-01-01 to 2016-12-31.\n",
    "\n",
    "There is a header line and each line of the file represents a record containing the following fields (separated by commas):\n",
    "- year: Year (format: YYYY)\n",
    "- industry1: Name of Industry (text)\n",
    "- industry2: Name of Sub-industry (text)\n",
    "- job_vacancy: Job Vacancy (numeric)\n",
    "\n",
    "The following are the first 10 lines of the data file:\n",
    "```\n",
    "year,industry1,industry2,job_vacancy\n",
    "1990,manufacturing,\"food, beverages and tobacco\",500\n",
    "1990,manufacturing,textile and wearing apparel,4700\n",
    "1990,manufacturing,paper products and publishing,1000\n",
    "1990,manufacturing,petroleum and chemical products,600\n",
    "1990,manufacturing,rubber and plastic products,1100\n",
    "1990,manufacturing,fabricated metal products,1700\n",
    "1990,manufacturing,machinery and equipment,1500\n",
    "1990,manufacturing,electrical products,900\n",
    "1990,manufacturing,electronic products,8500\n",
    "...\n",
    "```\n",
    "\n",
    "### Loading the data\n",
    "First, load the data into a DataFrame:\n",
    "```python\n",
    "vacancyDF = spark.read.csv(\"/home/training/data/job-vacancy-by-industry.csv\", inferSchema=True, header=True)\n",
    "```\n",
    "When reading the data, we have indicated that the data contains a **header** line by setting `header=True` and we would like Spark SQL to _infer_ the schema by setting `inferSchema=True`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "vacancyDF = spark.read.csv(\"/home/training/data/job-vacancy-by-industry.csv\", inferSchema=True, header=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can check whether the schema is inferred correctly by using the `printSchema` method as follows:\n",
    "```python\n",
    "vacancyDF.printSchema()\n",
    "```\n",
    "Notice that the `year` and `job_vacancy` fields are both set to `integer` while the rest are set to `string`. `nullable` equals to `true` indicates that the field can have missing values and missing values will be given a value of `null`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "root\n",
      " |-- year: integer (nullable = true)\n",
      " |-- industry1: string (nullable = true)\n",
      " |-- industry2: string (nullable = true)\n",
      " |-- job_vacancy: integer (nullable = true)\n",
      "\n"
     ]
    }
   ],
   "source": [
    "vacancyDF.printSchema()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Displaying the data\n",
    "We display the data using the `show` method as follows:\n",
    "```python\n",
    "vacancyDF.show()\n",
    "```\n",
    "By default, the first 20 rows are shown (you can indicate the number of rows to show by specifying a number as the argument e.g. `vacancyDF.show(5)` will show 5 rows only). If you do not want the content to be truncated, you can specify `truncate=False` in the argument of `show` e.g. `vacancyDF.show(truncate=False)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+----+-------------+--------------------+-----------+\n",
      "|year|    industry1|           industry2|job_vacancy|\n",
      "+----+-------------+--------------------+-----------+\n",
      "|1990|manufacturing|food, beverages a...|        500|\n",
      "|1990|manufacturing|textile and weari...|       4700|\n",
      "|1990|manufacturing|paper products an...|       1000|\n",
      "|1990|manufacturing|petroleum and che...|        600|\n",
      "|1990|manufacturing|rubber and plasti...|       1100|\n",
      "|1990|manufacturing|fabricated metal ...|       1700|\n",
      "|1990|manufacturing|machinery and equ...|       1500|\n",
      "|1990|manufacturing| electrical products|        900|\n",
      "|1990|manufacturing| electronic products|       8500|\n",
      "|1990|manufacturing|medical and preci...|        400|\n",
      "|1990|manufacturing| transport equipment|       2100|\n",
      "|1990|manufacturing|other manufacturi...|       1300|\n",
      "|1990| construction|        construction|       2200|\n",
      "|1990|     services|wholesale and ret...|       3800|\n",
      "|1990|     services|hotels and restau...|       4100|\n",
      "|1990|     services|transport, storag...|       2600|\n",
      "|1990|     services|financial interme...|       1500|\n",
      "|1990|     services|business and real...|       2000|\n",
      "|1990|     services|community, social...|       3100|\n",
      "|1990|       others|              others|        100|\n",
      "+----+-------------+--------------------+-----------+\n",
      "only showing top 20 rows\n",
      "\n"
     ]
    }
   ],
   "source": [
    "vacancyDF.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the `show` method does not return anything. It is intended only for viewing the content. Therefore, you **should not** assign the results of `show` to any variable. For example, in the following:\n",
    "```python\n",
    "results = vacancyDF.show()\n",
    "```\n",
    "Nothing will be stored in the variable `results` and `results` will be given a value of `None`.\n",
    "\n",
    "However, you can use the `first` method to retrieve the **first** record or the `collect` method to get **all** records (do not do this if your data is very large) or the `take` method to return a certain number of records e.g. the following retrieve the first three records and print them as a list of `Row` objects:\n",
    "```python\n",
    "print(vacancyDF.take(3))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[Row(year=1990, industry1='manufacturing', industry2='food, beverages and tobacco', job_vacancy=500), Row(year=1990, industry1='manufacturing', industry2='textile and wearing apparel', job_vacancy=4700), Row(year=1990, industry1='manufacturing', industry2='paper products and publishing', job_vacancy=1000)]\n"
     ]
    }
   ],
   "source": [
    "print(vacancyDF.take(3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Querying the data\n",
    "Use the `count` method to count the number of rows in the DataFrame:\n",
    "```python\n",
    "vacancyDF.count()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "523"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "vacancyDF.count()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can select specify rows using the `filter` clause e.g. to show all the records in the year 2016, do the following:\n",
    "```python\n",
    "vacancyDF.filter('year = 2016').show() # this also works: vacancyDF.filter(vacancyDF.year == 2016).show()\n",
    "```\n",
    "You can also use `where` in place of `filter`. They are the same."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+----+-------------+--------------------+-----------+\n",
      "|year|    industry1|           industry2|job_vacancy|\n",
      "+----+-------------+--------------------+-----------+\n",
      "|2016|manufacturing|food, beverages a...|        600|\n",
      "|2016|manufacturing|paper,rubber,plas...|        300|\n",
      "|2016|manufacturing|petroleum, chemic...|        400|\n",
      "|2016|manufacturing|fabricated metal ...|       1000|\n",
      "|2016|manufacturing|electronic, compu...|       1900|\n",
      "|2016|manufacturing| transport equipment|        800|\n",
      "|2016|manufacturing|other manufacturi...|        700|\n",
      "|2016| construction|        construction|       2000|\n",
      "|2016|     services|wholesale and ret...|       6200|\n",
      "|2016|     services|transportation an...|       2900|\n",
      "|2016|     services|accommodation and...|       5700|\n",
      "|2016|     services|information and c...|       2900|\n",
      "|2016|     services|financial and ins...|       3200|\n",
      "|2016|     services|real estate services|       1600|\n",
      "|2016|     services|professional serv...|       3300|\n",
      "|2016|     services|administrative an...|       4900|\n",
      "|2016|     services|community, social...|      10800|\n",
      "|2016|       others|              others|        400|\n",
      "+----+-------------+--------------------+-----------+\n",
      "\n"
     ]
    }
   ],
   "source": [
    "vacancyDF.filter('year = 2016').show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us work with columns. You can display the list of columns using the `columns` method as follows:\n",
    "```python\n",
    "vacancyDF.columns\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['year', 'industry1', 'industry2', 'job_vacancy']"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "vacancyDF.columns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can select specific columns using the `select` method e.g. to get a new DataFrame containing only the `year`, `industry1` and `job_vacancy` columns:\n",
    "```python\n",
    "vacancyDF.select(\"year\", \"industry1\", \"job_vacancy\").show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+----+-------------+-----------+\n",
      "|year|    industry1|job_vacancy|\n",
      "+----+-------------+-----------+\n",
      "|1990|manufacturing|        500|\n",
      "|1990|manufacturing|       4700|\n",
      "|1990|manufacturing|       1000|\n",
      "|1990|manufacturing|        600|\n",
      "|1990|manufacturing|       1100|\n",
      "|1990|manufacturing|       1700|\n",
      "|1990|manufacturing|       1500|\n",
      "|1990|manufacturing|        900|\n",
      "|1990|manufacturing|       8500|\n",
      "|1990|manufacturing|        400|\n",
      "|1990|manufacturing|       2100|\n",
      "|1990|manufacturing|       1300|\n",
      "|1990| construction|       2200|\n",
      "|1990|     services|       3800|\n",
      "|1990|     services|       4100|\n",
      "|1990|     services|       2600|\n",
      "|1990|     services|       1500|\n",
      "|1990|     services|       2000|\n",
      "|1990|     services|       3100|\n",
      "|1990|       others|        100|\n",
      "+----+-------------+-----------+\n",
      "only showing top 20 rows\n",
      "\n"
     ]
    }
   ],
   "source": [
    "vacancyDF.select(\"year\", \"industry1\", \"job_vacancy\").show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the `distinct` method to return a new DataFrame containing the distinct rows in the input DataFrame, essentially filtering out duplicate rows e.g. to get all the distinct combination of industry1 and industry2 values, do the following:\n",
    "```python\n",
    "vacancyDF.select(vacancyDF.industry1, vacancyDF.industry2).distinct().show(truncate=False)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+-------------+--------------------------------------------------+\n",
      "|industry1    |industry2                                         |\n",
      "+-------------+--------------------------------------------------+\n",
      "|services     |financial intermediation                          |\n",
      "|manufacturing|fabricated metal products                         |\n",
      "|manufacturing|electronic products                               |\n",
      "|manufacturing|paper products and publishing                     |\n",
      "|manufacturing|paper,rubber,plastic products and printing        |\n",
      "|services     |transport and storage                             |\n",
      "|services     |hotels and restaurants                            |\n",
      "|services     |information and communications                    |\n",
      "|services     |wholesale and retail trade                        |\n",
      "|manufacturing|fabricated metal products, machinery and equipment|\n",
      "|manufacturing|rubber and plastic products                       |\n",
      "|manufacturing|machinery and equipment                           |\n",
      "|others       |others                                            |\n",
      "|services     |accommodation and food services                   |\n",
      "|construction |construction                                      |\n",
      "|manufacturing|other manufacturing industries                    |\n",
      "|services     |financial services                                |\n",
      "|services     |real estate and leasing services                  |\n",
      "|services     |financial and insurance services                  |\n",
      "|manufacturing|medical and precision instruments                 |\n",
      "+-------------+--------------------------------------------------+\n",
      "only showing top 20 rows\n",
      "\n"
     ]
    }
   ],
   "source": [
    "vacancyDF.select(vacancyDF.industry1, vacancyDF.industry2).distinct().show(truncate=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also choose to create a new DataFrame from an existing one without certain columns using the `drop` method. For example, to drop the column \"industry2\", do the following:\n",
    "```python\n",
    "vacancyDF.drop(\"industry2\").show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+----+-------------+-----------+\n",
      "|year|    industry1|job_vacancy|\n",
      "+----+-------------+-----------+\n",
      "|1990|manufacturing|        500|\n",
      "|1990|manufacturing|       4700|\n",
      "|1990|manufacturing|       1000|\n",
      "|1990|manufacturing|        600|\n",
      "|1990|manufacturing|       1100|\n",
      "|1990|manufacturing|       1700|\n",
      "|1990|manufacturing|       1500|\n",
      "|1990|manufacturing|        900|\n",
      "|1990|manufacturing|       8500|\n",
      "|1990|manufacturing|        400|\n",
      "|1990|manufacturing|       2100|\n",
      "|1990|manufacturing|       1300|\n",
      "|1990| construction|       2200|\n",
      "|1990|     services|       3800|\n",
      "|1990|     services|       4100|\n",
      "|1990|     services|       2600|\n",
      "|1990|     services|       1500|\n",
      "|1990|     services|       2000|\n",
      "|1990|     services|       3100|\n",
      "|1990|       others|        100|\n",
      "+----+-------------+-----------+\n",
      "only showing top 20 rows\n",
      "\n"
     ]
    }
   ],
   "source": [
    "vacancyDF.drop(\"industry2\").show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us try something more complex by _chaining_ several operations together. Display the list of sub-industries whose number of vacancies is more than 1000 in the year 2016:\n",
    "```python\n",
    "vacancyDF.select(\"industry2\").where(\"job_vacancy > 1000 and year = 2016\").show(truncate=False)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+-----------------------------------------+\n",
      "|industry2                                |\n",
      "+-----------------------------------------+\n",
      "|electronic, computer and optical products|\n",
      "|construction                             |\n",
      "|wholesale and retail trade               |\n",
      "|transportation and storage               |\n",
      "|accommodation and food services          |\n",
      "|information and communications           |\n",
      "|financial and insurance services         |\n",
      "|real estate services                     |\n",
      "|professional services                    |\n",
      "|administrative and support services      |\n",
      "|community, social and personal services  |\n",
      "+-----------------------------------------+\n",
      "\n"
     ]
    }
   ],
   "source": [
    "vacancyDF.select(\"industry2\").where(\"job_vacancy > 1000 and year = 2016\").show(truncate=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sorting and Ordering DataFrames\n",
    "You can sort the content of a DataFrame using the `orderBy` method (`orderby` is just an alias for the `sort` method). For example, to display the data sorted in descending order of job vacancies, do the following:\n",
    "```python\n",
    "vacancyDF.orderBy(\"job_vacancy\", ascending=False).show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+----+-------------+--------------------+-----------+\n",
      "|year|    industry1|           industry2|job_vacancy|\n",
      "+----+-------------+--------------------+-----------+\n",
      "|2014|     services|community, social...|      13700|\n",
      "|2015|     services|community, social...|      12600|\n",
      "|2011|     services|community, social...|      12600|\n",
      "|2013|     services|community, social...|      12400|\n",
      "|2012|     services|community, social...|      11200|\n",
      "|2016|     services|community, social...|      10800|\n",
      "|2010|     services|community, social...|      10600|\n",
      "|2009|     services|community, social...|      10200|\n",
      "|2008|     services|community, social...|       9300|\n",
      "|1990|manufacturing| electronic products|       8500|\n",
      "|2014|     services|wholesale and ret...|       7900|\n",
      "|2007|     services|community, social...|       7400|\n",
      "|2014|     services|accommodation and...|       7400|\n",
      "|2015|     services|wholesale and ret...|       7200|\n",
      "|1997| construction|        construction|       7200|\n",
      "|2015|     services|accommodation and...|       7200|\n",
      "|1995|manufacturing| electronic products|       7100|\n",
      "|2013|     services|wholesale and ret...|       7000|\n",
      "|1996| construction|        construction|       6800|\n",
      "|1994|manufacturing| electronic products|       6200|\n",
      "+----+-------------+--------------------+-----------+\n",
      "only showing top 20 rows\n",
      "\n"
     ]
    }
   ],
   "source": [
    "vacancyDF.orderBy(\"job_vacancy\", ascending=False).show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us try another one. Display the sub-industries of the services industry in descending order of job vacancies in the year 2016:\n",
    "```python\n",
    "vacancyDF.where((vacancyDF.year == 2016) & (vacancyDF.industry1 == \"services\")) \\\n",
    "    .select(\"industry2\", \"job_vacancy\") \\\n",
    "    .orderBy(\"job_vacancy\", ascending=False).show(truncate=False)\n",
    "```\n",
    "Note: When your statement is long, you can break the code into multiple lines using the `\\` as shown above. However,\n",
    "do not do this to break a string into multiple lines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+---------------------------------------+-----------+\n",
      "|industry2                              |job_vacancy|\n",
      "+---------------------------------------+-----------+\n",
      "|community, social and personal services|10800      |\n",
      "|wholesale and retail trade             |6200       |\n",
      "|accommodation and food services        |5700       |\n",
      "|administrative and support services    |4900       |\n",
      "|professional services                  |3300       |\n",
      "|financial and insurance services       |3200       |\n",
      "|transportation and storage             |2900       |\n",
      "|information and communications         |2900       |\n",
      "|real estate services                   |1600       |\n",
      "+---------------------------------------+-----------+\n",
      "\n"
     ]
    }
   ],
   "source": [
    "vacancyDF.where((vacancyDF.year == 2016) & (vacancyDF.industry1 == \"services\")).select(\"industry2\", \"job_vacancy\") \\\n",
    "    .orderBy(\"job_vacancy\", ascending=False).show(truncate=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Grouping is a common precursor to performing aggregations on a column or columns in a DataFrame. The DataFrame API includes the `groupBy` method, which is used to group the DataFrame on specific columns. This method returns a `pyspark.sql.GroupedData` object, which is a special type of DataFrame containing grouped data that allows common aggregate functions such as sum and count to be used on the grouped data. For example, let us group the job vacancies by year and find the total number of vacancies for each year:\n",
    "```python\n",
    "vacancyDF.groupBy(\"year\").sum('job_vacancy').show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+----+----------------+\n",
      "|year|sum(job_vacancy)|\n",
      "+----+----------------+\n",
      "|1990|           43700|\n",
      "|2003|           12600|\n",
      "|2007|           38800|\n",
      "|2015|           59300|\n",
      "|2006|           31100|\n",
      "|2013|           55500|\n",
      "|1997|           42100|\n",
      "|1994|           43700|\n",
      "|2014|           63100|\n",
      "|2004|           16300|\n",
      "|1991|           38200|\n",
      "|1996|           44200|\n",
      "|1998|           18400|\n",
      "|2012|           48500|\n",
      "|2009|           30400|\n",
      "|2016|           49600|\n",
      "|1995|           46300|\n",
      "|2001|           15800|\n",
      "|1992|           38200|\n",
      "|2005|           19400|\n",
      "+----+----------------+\n",
      "only showing top 20 rows\n",
      "\n"
     ]
    }
   ],
   "source": [
    "vacancyDF.groupBy(\"year\").sum('job_vacancy').show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Great! You have written your first Spark program. You may want to save your notebook by clicking on the save icon (the first icon on the left of the toolbar). You will also want to download a HTML version by selecting File -> Download as -> HTML (.html) so that you have a copy of the source code that can be viewed easily (note that the html file will be stored in the `Downloads` folder).\n",
    "\n",
    "Exit this program by selecting File -> Close and Halt. **Do not close the browser tab to exit the program**."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
