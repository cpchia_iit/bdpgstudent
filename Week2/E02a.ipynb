{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## E-Learning 2a - Using Spark SQL for Data Processing\n",
    "Apache Spark is an open-source powerful distributed querying and processing engine. From this point on, we will be writing Python programs that will be accessing the Spark APIs so that we can process big data efficiently. We will be writing our Spark programs using the Jupyter Notebook so that we can perform our analytics interactively.\n",
    "\n",
    "In this first exercise, you will learn to how write your programs using Spark SQL, one of the most advanced components of Apache Spark. When you type `pyspark` at the command line, the Spark engine automatically creates a `SparkSession` object for you called `spark`. This object represents a unified entry point for manipulating data in Spark. Let us first make sure that this object exists by typing `spark` in the cell below.\n",
    "```python\n",
    "spark\n",
    "```\n",
    "Note that depending on the speed of your notebook, it might take a few seconds before any output appears. You should see that the version of the Spark engine is v2.3.2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To query and process structured data using Spark SQL, we need to impose a structure called **DataFrame** onto a distributed collection of data. The DataFrame is an immutable distributed collection of data that is organised into named columns analogous to a table in a relational database. Before we create our first DataFrame, let us look at the dataset we will be using. The data file is `job-vacancy-by-industry.csv` and is located in the `/home/training/data` directory. It contains information of job vacancies for various industries (including sub-industries) from 1990-01-01 to 2016-12-31.\n",
    "\n",
    "There is a header line and each line of the file represents a record containing the following fields (separated by commas):\n",
    "- year: Year (format: YYYY)\n",
    "- industry1: Name of Industry (text)\n",
    "- industry2: Name of Sub-industry (text)\n",
    "- job_vacancy: Job Vacancy (numeric)\n",
    "\n",
    "The following are the first 10 lines of the data file:\n",
    "```\n",
    "year,industry1,industry2,job_vacancy\n",
    "1990,manufacturing,\"food, beverages and tobacco\",500\n",
    "1990,manufacturing,textile and wearing apparel,4700\n",
    "1990,manufacturing,paper products and publishing,1000\n",
    "1990,manufacturing,petroleum and chemical products,600\n",
    "1990,manufacturing,rubber and plastic products,1100\n",
    "1990,manufacturing,fabricated metal products,1700\n",
    "1990,manufacturing,machinery and equipment,1500\n",
    "1990,manufacturing,electrical products,900\n",
    "1990,manufacturing,electronic products,8500\n",
    "...\n",
    "```\n",
    "\n",
    "### Loading the data\n",
    "First, load the data into a DataFrame:\n",
    "```python\n",
    "vacancyDF = spark.read.csv(\"/home/training/data/job-vacancy-by-industry.csv\", inferSchema=True, header=True)\n",
    "```\n",
    "When reading the data, we have indicated that the data contains a **header** line by setting `header=True` and we would like Spark SQL to _infer_ the schema by setting `inferSchema=True`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can check whether the schema is inferred correctly by using the `printSchema` method as follows:\n",
    "```python\n",
    "vacancyDF.printSchema()\n",
    "```\n",
    "Notice that the `year` and `job_vacancy` fields are both set to `integer` while the rest are set to `string`. `nullable` equals to `true` indicates that the field can have missing values and missing values will be given a value of `null`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Displaying the data\n",
    "We display the data using the `show` method as follows:\n",
    "```python\n",
    "vacancyDF.show()\n",
    "```\n",
    "By default, the first 20 rows are shown (you can indicate the number of rows to show by specifying a number as the argument e.g. `vacancyDF.show(5)` will show 5 rows only). If you do not want the content to be truncated, you can specify `truncate=False` in the argument of `show` e.g. `vacancyDF.show(truncate=False)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the `show` method does not return anything. It is intended only for viewing the content. Therefore, you **should not** assign the results of `show` to any variable. For example, in the following:\n",
    "```python\n",
    "results = vacancyDF.show()\n",
    "```\n",
    "Nothing will be stored in the variable `results` and `results` will be given a value of `None`.\n",
    "\n",
    "However, you can use the `first` method to retrieve the **first** record or the `collect` method to get **all** records (do not do this if your data is very large) or the `take` method to return a certain number of records e.g. the following retrieve the first three records and print them as a list of `Row` objects:\n",
    "```python\n",
    "print(vacancyDF.take(3))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Querying the data\n",
    "Use the `count` method to count the number of rows in the DataFrame:\n",
    "```python\n",
    "vacancyDF.count()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can select specify rows using the `filter` clause e.g. to show all the records in the year 2016, do the following:\n",
    "```python\n",
    "vacancyDF.filter('year = 2016').show() # this also works: vacancyDF.filter(vacancyDF.year == 2016).show()\n",
    "```\n",
    "You can also use `where` in place of `filter`. They are the same."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us work with columns. You can display the list of columns using the `columns` method as follows:\n",
    "```python\n",
    "vacancyDF.columns\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can select specific columns using the `select` method e.g. to get a new DataFrame containing only the `year`, `industry1` and `job_vacancy` columns:\n",
    "```python\n",
    "vacancyDF.select(\"year\", \"industry1\", \"job_vacancy\").show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the `distinct` method to return a new DataFrame containing the distinct rows in the input DataFrame, essentially filtering out duplicate rows e.g. to get all the distinct combination of industry1 and industry2 values, do the following:\n",
    "```python\n",
    "vacancyDF.select(vacancyDF.industry1, vacancyDF.industry2).distinct().show(truncate=False)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also choose to create a new DataFrame from an existing one without certain columns using the `drop` method. For example, to drop the column \"industry2\", do the following:\n",
    "```python\n",
    "vacancyDF.drop(\"industry2\").show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us try something more complex by _chaining_ several operations together. Display the list of sub-industries whose number of vacancies is more than 1000 in the year 2016:\n",
    "```python\n",
    "vacancyDF.select(\"industry2\").where(\"job_vacancy > 1000 and year = 2016\").show(truncate=False)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sorting and Ordering DataFrames\n",
    "You can sort the content of a DataFrame using the `orderBy` method (`orderby` is just an alias for the `sort` method). For example, to display the data sorted in descending order of job vacancies, do the following:\n",
    "```python\n",
    "vacancyDF.orderBy(\"job_vacancy\", ascending=False).show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us try another one. Display the sub-industries of the services industry in descending order of job vacancies in the year 2016:\n",
    "```python\n",
    "vacancyDF.where((vacancyDF.year == 2016) & (vacancyDF.industry1 == \"services\")) \\\n",
    "    .select(\"industry2\", \"job_vacancy\") \\\n",
    "    .orderBy(\"job_vacancy\", ascending=False).show(truncate=False)\n",
    "```\n",
    "Note: When your statement is long, you can break the code into multiple lines using the `\\` as shown above. However,\n",
    "do not do this to break a string into multiple lines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Grouping is a common precursor to performing aggregations on a column or columns in a DataFrame. The DataFrame API includes the `groupBy` method, which is used to group the DataFrame on specific columns. This method returns a `pyspark.sql.GroupedData` object, which is a special type of DataFrame containing grouped data that allows common aggregate functions such as sum and count to be used on the grouped data. For example, let us group the job vacancies by year and find the total number of vacancies for each year:\n",
    "```python\n",
    "vacancyDF.groupBy(\"year\").sum('job_vacancy').show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Great! You have written your first Spark program. You may want to save your notebook by clicking on the save icon (the first icon on the left of the toolbar). You will also want to download a HTML version by selecting File -> Download as -> HTML (.html) so that you have a copy of the source code that can be viewed easily (note that the html file will be stored in the `Downloads` folder).\n",
    "\n",
    "Exit this program by selecting File -> Close and Halt. **Do not close the browser tab to exit the program**."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
