{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Practical 4a - K-means Clustering\n",
    "In this exercise, we are going to implement *clustering*. The task of clustering is to group a set of data points into several groups (clusters), based on some similarity metric. Clustering is an unsupervised learning method: a clustering algorithm learns the labels itself. For instance, a classification algorithm gets a set of images labeled cats and dogs, and it learns how to recognise cats and dogs on future images. A clustering algorithm can try to spot differences between different images and automatically categorize them into two groups, but not knowing the names for each group. At most it can label them \"group 1\" and \"group 2\". \n",
    "\n",
    "In this exercise, we shall apply a clustering algorithm called k-means clustering which is already covered in the lecture. We shall use our resale flat data `resale-flat-prices.csv` and derive clusters based on the floor area and resale price. Recall that the fields are delimited by commas and there is a header line indicating names of the fields:\n",
    "```\n",
    "month: Month of registration for the resale transactions (format: YYYY-MM)\n",
    "town: Town the flat is in\n",
    "flat_type: Type of flat\n",
    "block: The block number\n",
    "street_name: The street name\n",
    "storey_range: The storey range \n",
    "floor_area_sqm: Floor area in square metres\n",
    "flat_model: The flat model\n",
    "lease_commence_date: The lease commence date (format: YYYY)\n",
    "resale_price: The resale price in Singapore dollars\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Loading the Data\n",
    "First, create the schema for the dataset and load the dataset into a DataFrame. Since you will be working on the floor area and resale price, select both fields.\n",
    "```python\n",
    "from pyspark.sql.types import *\n",
    "\n",
    "schema = StructType( [\n",
    "    StructField('month', StringType(), True),\n",
    "    StructField('town', StringType(), True),\n",
    "    StructField('flat_type', StringType(), True),\n",
    "    StructField('block', StringType(), True),\n",
    "    StructField('street_name', StringType(), True),\n",
    "    StructField('storey_range', StringType(), True),\n",
    "    StructField('floor_area', DoubleType(), True),\n",
    "    StructField('flat_model', StringType(), True),    \n",
    "    StructField('lease_commence_date', DoubleType(), True),\n",
    "    StructField('resale_price', DoubleType(), True) \n",
    "] )\n",
    "\n",
    "flats = spark.read.csv('/home/training/data/resale-flat-prices.csv', header=True, schema=schema)\n",
    "flatsDF = flats.select(\"floor_area\", \"resale_price\").distinct()\n",
    "flatsDF.persist()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating transformers\n",
    "Since the scale of floor area (in square metres) is different from resale price (in Singapore dollars), it would be good to scale both of these features into a standard scale first.\n",
    "```python\n",
    "import pyspark.ml.feature as ft\n",
    "\n",
    "assembler = ft.VectorAssembler(inputCols=[\"floor_area\", \"resale_price\"], outputCol=\"features-vec\")\n",
    "scaler = ft.StandardScaler(inputCol=\"features-vec\", outputCol=\"features\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating an Estimator\n",
    "We shall use the k-means model with 5 clusters.\n",
    "```python\n",
    "import pyspark.ml.clustering as clus\n",
    "kmeans = clus.KMeans(k = 5, featuresCol = 'features')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating a Pipeline\n",
    "Now, create a Pipeline to pull the different transformations together:\n",
    "```python\n",
    "from pyspark.ml import Pipeline\n",
    "pipeline = Pipeline( stages = [assembler, scaler, kmeans] )\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fitting the Model\n",
    "With clustering algorithms, there is no point in having a validation and a training dataset. So, we shall use the entire dataset.\n",
    "```python\n",
    "model = pipeline.fit(flatsDF)\n",
    "clusters = model.transform(flatsDF)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us take a closer look at the k-means model that is generated.\n",
    "```python\n",
    "km = model.stages[2]\n",
    "summary = km.summary\n",
    "print(\"Number of clusters: \", summary.k)\n",
    "print(\"Size of (number of data points in) each cluster: \", summary.clusterSizes)\n",
    "print(\"The cluster centers are: \", km.clusterCenters())\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can plot the clusters to visually see them.\n",
    "```python\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "\n",
    "plt.style.use('ggplot')\n",
    "plt.figure(figsize=(20,10))\n",
    "\n",
    "for i in range(5):\n",
    "    ax = sns.regplot(x=\"floor_area\", y=\"resale_price\", label=\"Cluster \" + str(i+1),\n",
    "                     data=clusters.select(\"floor_area\", \"resale_price\"). \\\n",
    "                     filter(clusters.prediction == i).toPandas(), fit_reg=False)\n",
    "ax.set_xlabel(\"Floor Area (sqm)\", size=14)\n",
    "ax.set_ylabel(\"Resale Price (S$)\", size=14)\n",
    "ax.set_title(\"After clustering\", size=20)\n",
    "ax.legend(loc=4, fontsize=14)\n",
    "\n",
    "plt.show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluating the Performance of the Model\n",
    "Evaluating clustering models can be difficult because of the nature of the clustering problem: clusters are not known in advance, and it is not easy to separate good and bad clusters. A clustering could be considered good if each data point is near to its closest centroid, where \"near\" is defined by the Euclidean distance. This is a simple, common way to evaluate the quality of a clustering, by the mean of these distances over all points, or sometimes, the mean of the distances squared. In fact, `KMeansModel` offers a `computeCost` method which computes the sum of squared distances, and can easily be used to compute the mean squared distance.\n",
    "```python\n",
    "km.computeCost(clusters) / flatsDF.count()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
