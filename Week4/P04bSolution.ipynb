{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Practical 4b - Logistic Regression (Solution)\n",
    "Logistic Regression is a classification algorithm and its sole purpose is to classify input data points (consisting of input variables' values) into two or more classes. In this exercise, we shall focus only on binary logistic regression models which will only classify examples into two possible classes. While we will not be going through the mathematics behind the approach, you need to remember that logistic regression outputs a probability p(x) that a data point x (a vector) belongs to a specific class.\n",
    "\n",
    "We shall reuse the Wisconsin Breast Cancer dataset which we used for building decision tree from your e-learning exercise. The filename is called `breast-cancer-wisconsin.csv` and is located in the `data` directory. Recall that the dataset contains 699 records, with 458 classified as benign tumors and 241 as malignant cases. Each record is described by nine attributes with an integer value in the range of 1 to 10 and a binary class label. The attributes, each separated by a comma, are as follows:\n",
    "\n",
    "| No.   | Attribute                     | Domain    |\n",
    "|:-----:| :---------------------------: | :---------: |\n",
    "| 1     | Sample code number            | ID number |\n",
    "| 2     | Clump Thickness               | 1 - 10    |\n",
    "| 3     | Uniformity of Cell Size       | 1 - 10    |\n",
    "| 4     | Uniformity of Cell Shape      | 1 - 10    |\n",
    "| 5     | Marginal Adhesion             | 1 - 10    |\n",
    "| 6     | Single Epithelial Cell Size   | 1 - 10    |\n",
    "| 7     | Bare Nuclei                   | 1 - 10    |\n",
    "| 8     | Bland Chromatin               | 1 - 10    |\n",
    "| 9     | Normal Nucleoli               | 1 - 10    |\n",
    "| 10    | Mitoses                       | 1 - 10    |\n",
    "| 11    | Class                         | (2 for benign, 4 for Malignant) |\n",
    "\n",
    "We shall use the breast cancer data and fit a logistics regression model using a binary classification to train and predict the label (benign (0.0) and malignant (1.0)) for the dataset.\n",
    "\n",
    "### Loading the Data\n",
    "Let us first create the schema and load the data into a DataFrame:\n",
    "```python\n",
    "from pyspark.sql.types import *\n",
    "\n",
    "schema = StructType( [\n",
    "    StructField(\"Sample Code Number\", StringType(), True),\n",
    "    StructField(\"Clump Thickness\", DoubleType(), True),\n",
    "    StructField(\"Uniformity of Cell Size\", DoubleType(), True),\n",
    "    StructField(\"Uniformity of Cell Shape\", DoubleType(), True),\n",
    "    StructField(\"Marginal Adhesion\", DoubleType(), True),\n",
    "    StructField(\"Single Epithelial Cell Size\", DoubleType(), True),\n",
    "    StructField(\"Bare Nuclei\", DoubleType(), True),\n",
    "    StructField(\"Bland Chromatin\", DoubleType(), True),\n",
    "    StructField(\"Normal Nucleoli\", DoubleType(), True),\n",
    "    StructField(\"Mitoses\", DoubleType(), True),\n",
    "    StructField(\"Class\", DoubleType(), True)\n",
    "])\n",
    "cancer_df = spark.read.csv(\"/home/training/data/breast-cancer-wisconsin.csv\", schema=schema)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.sql.types import *\n",
    "\n",
    "schema = StructType( [\n",
    "    StructField(\"Sample Code Number\", StringType(), True),\n",
    "    StructField(\"Clump Thickness\", DoubleType(), True),\n",
    "    StructField(\"Uniformity of Cell Size\", DoubleType(), True),\n",
    "    StructField(\"Uniformity of Cell Shape\", DoubleType(), True),\n",
    "    StructField(\"Marginal Adhesion\", DoubleType(), True),\n",
    "    StructField(\"Single Epithelial Cell Size\", DoubleType(), True),\n",
    "    StructField(\"Bare Nuclei\", DoubleType(), True),\n",
    "    StructField(\"Bland Chromatin\", DoubleType(), True),\n",
    "    StructField(\"Normal Nucleoli\", DoubleType(), True),\n",
    "    StructField(\"Mitoses\", DoubleType(), True),\n",
    "    StructField(\"Class\", DoubleType(), True)\n",
    "])\n",
    "cancer_df = spark.read.csv(\"/home/training/data/breast-cancer-wisconsin.csv\", schema=schema)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, we will remove the `Sample Code Number` column, the records containing missing attribute values and convert the values for benign cases from 2 to 0 and the values for malignant cases from 4 to 1. The code is as follows:\n",
    "```python\n",
    "cancer_clean_df = cancer_df.drop('Sample Code Number').dropna().replace([2.0, 4.0], [0.0, 1.0], 'Class')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "cancer_clean_df = cancer_df.drop('Sample Code Number').dropna().replace([2.0, 4.0], [0.0, 1.0], 'Class')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating transformers\n",
    "We shall use the `VectorAssembler` to assemble the values from all the input columns into the output column:\n",
    "```python\n",
    "import pyspark.ml.feature as ft\n",
    "featuresCreator = ft.VectorAssembler(inputCols=[\"Clump Thickness\", \"Uniformity of Cell Size\",\n",
    "                                                \"Uniformity of Cell Shape\", \"Marginal Adhesion\",\n",
    "                                                \"Single Epithelial Cell Size\", \"Bare Nuclei\",\n",
    "                                                \"Bland Chromatin\", \"Normal Nucleoli\", \"Mitoses\"], \n",
    "                                                outputCol='features')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.feature as ft\n",
    "featuresCreator = ft.VectorAssembler(inputCols=[\"Clump Thickness\", \"Uniformity of Cell Size\",\n",
    "                                                \"Uniformity of Cell Shape\", \"Marginal Adhesion\",\n",
    "                                                \"Single Epithelial Cell Size\", \"Bare Nuclei\",\n",
    "                                                \"Bland Chromatin\", \"Normal Nucleoli\", \"Mitoses\"], outputCol='features')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating an Estimator\n",
    "Next, we create our logistic regression model. \n",
    "```python\n",
    "import pyspark.ml.classification as cl\n",
    "lr = cl.LogisticRegression(regParam=0.01, maxIter=50, elasticNetParam=0.01, labelCol=\"Class\")\n",
    "```\n",
    "`maxIter` is the number of iterations the algorithm will run while `elasticNetParam` and `regParam` are used to handle overfitting. Overfitting is a common problem in machine learning where a model performs well on training data but does not generalize well to unseen data (test data). A way to mitigate over-fitting in logistic regression is to use regularization: we impose a penalty for large values of the parameters when optimizing. Spark has two parameters that can be tweaked to control regularization:\n",
    "- The type of regularization, set with the `elasticNetParam` parameter. A value of 0 indicates L2-norm (a type of loss function) while a value of 1 indicates L1-norm regularization. Anything in-between will be using a combination of L1 and L2 regularization.  \n",
    "- The degree of regularization, set with the `regParam` parameter. A high value of the regularization parameter indicates a strong regularization. In general, the greater the danger of over-fitting, the larger the regularization parameter ought to be.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.classification as cl\n",
    "lr = cl.LogisticRegression(regParam=0.01, maxIter=50, elasticNetParam=0.01, labelCol=\"Class\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating a Pipeline\n",
    "Now, create a Pipeline to pull the different transformations together:\n",
    "```python\n",
    "from pyspark.ml import Pipeline\n",
    "pipeline = Pipeline( stages = [featuresCreator, lr] )\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml import Pipeline\n",
    "pipeline = Pipeline( stages = [featuresCreator, lr] )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fitting the Model\n",
    "Before you fit the model, we shall split our dataset into training and testing datasets using the `randomSplit` method:\n",
    "```python\n",
    "train_data, test_data = cancer_clean_df.randomSplit([0.7, 0.3], seed=1234567)\n",
    "train_data.persist()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "DataFrame[Clump Thickness: double, Uniformity of Cell Size: double, Uniformity of Cell Shape: double, Marginal Adhesion: double, Single Epithelial Cell Size: double, Bare Nuclei: double, Bland Chromatin: double, Normal Nucleoli: double, Mitoses: double, Class: double]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "train_data, test_data = cancer_clean_df.randomSplit([0.7, 0.3], seed=1234567)\n",
    "train_data.persist()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, run our pipeline to estimate our model:\n",
    "```python\n",
    "model = pipeline.fit(train_data)\n",
    "test_model = model.transform(test_data)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = pipeline.fit(train_data)\n",
    "test_model = model.transform(test_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluating the Performance of the Model\n",
    "To evaluate the performance of our model, we can use the `BinaryClassificationEvaluator` class and its `evaluate` method:\n",
    "```python\n",
    "import pyspark.ml.evaluation as ev\n",
    "evaluator = ev.BinaryClassificationEvaluator(rawPredictionCol=\"probability\", labelCol=\"Class\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.evaluation as ev\n",
    "evaluator = ev.BinaryClassificationEvaluator(rawPredictionCol=\"probability\", labelCol=\"Class\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us see how well our model performed:\n",
    "```python\n",
    "print(evaluator.evaluate(test_model, {evaluator.metricName: 'areaUnderROC'}))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.9981247069854665\n"
     ]
    }
   ],
   "source": [
    "print(evaluator.evaluate(test_model, {evaluator.metricName: 'areaUnderROC'}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The nearer the resulting value is to 1, the more accurate your model is."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
