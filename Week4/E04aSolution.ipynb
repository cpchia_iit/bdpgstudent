{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## E-Learning 4a - Decision Trees (Solution)\n",
    "Decision trees are one of the oldest and more widely used methods of machine learning. What makes them popular is not only their ability to deal with more complex partitioning and segmentation but also their ability to explain how we arrived at a solution and as to \"why\" the outcome is classified as a class/label.\n",
    "\n",
    "In this exercise, we shall use the Wisconsin Breast Cancer dataset which was obtained from the University of Wisconsin Hospital from Dr. William H Wolberg. The Wisconsin Breast Cancer dataset is widely used in the machine learning community. The dataset contains limited attributes and most of them are discrete numbers. The file name is called `breast-cancer-wisconsin.csv` and is located in the `data` directory. The dataset currently contains clinical cases from 1989 to 1991. It has 699 records, with 458 classified as benign tumors and 241 as malignant cases. Each record is described by nine attributes with an integer value in the range of 1 to 10 and a binary class label. The attributes, each separated by a comma, are as follows:\n",
    "\n",
    "| No.   | Attribute                     | Domain    |\n",
    "|:-----:| :---------------------------: | :---------: |\n",
    "| 1     | Sample code number            | ID number |\n",
    "| 2     | Clump Thickness               | 1 - 10    |\n",
    "| 3     | Uniformity of Cell Size       | 1 - 10    |\n",
    "| 4     | Uniformity of Cell Shape      | 1 - 10    |\n",
    "| 5     | Marginal Adhesion             | 1 - 10    |\n",
    "| 6     | Single Epithelial Cell Size   | 1 - 10    |\n",
    "| 7     | Bare Nuclei                   | 1 - 10    |\n",
    "| 8     | Bland Chromatin               | 1 - 10    |\n",
    "| 9     | Normal Nucleoli               | 1 - 10    |\n",
    "| 10    | Mitoses                       | 1 - 10    |\n",
    "| 11    | Class                         | (2 for benign, 4 for Malignant) |\n",
    "\n",
    "The first 10 lines of the data file is as follows:\n",
    "```\n",
    "1000025,5,1,1,1,2,1,3,1,1,2\n",
    "1002945,5,4,4,5,7,10,3,2,1,2\n",
    "1015425,3,1,1,1,2,2,3,1,1,2\n",
    "1016277,6,8,8,1,3,4,3,7,1,2\n",
    "1017023,4,1,1,3,2,1,3,1,1,2\n",
    "1017122,8,10,10,8,7,10,9,7,1,4\n",
    "1018099,1,1,1,1,2,10,3,1,1,2\n",
    "1018561,2,1,2,1,2,1,3,1,1,2\n",
    "1033078,2,1,1,1,2,1,1,1,5,2\n",
    "1033078,4,2,1,1,2,1,2,1,1,2\n",
    "```\n",
    "\n",
    "We shall use the breast cancer data and fit a decision tree using a binary classification to train and predict the label (benign (0.0) and malignant (1.0)) for the dataset.\n",
    "\n",
    "### Loading the Data\n",
    "Let us first create the schema and load the data into a DataFrame:\n",
    "```python\n",
    "from pyspark.sql.types import *\n",
    "\n",
    "schema = StructType( [\n",
    "    StructField(\"Sample Code Number\", StringType(), True),\n",
    "    StructField(\"Clump Thickness\", DoubleType(), True),\n",
    "    StructField(\"Uniformity of Cell Size\", DoubleType(), True),\n",
    "    StructField(\"Uniformity of Cell Shape\", DoubleType(), True),\n",
    "    StructField(\"Marginal Adhesion\", DoubleType(), True),\n",
    "    StructField(\"Single Epithelial Cell Size\", DoubleType(), True),\n",
    "    StructField(\"Bare Nuclei\", DoubleType(), True),\n",
    "    StructField(\"Bland Chromatin\", DoubleType(), True),\n",
    "    StructField(\"Normal Nucleoli\", DoubleType(), True),\n",
    "    StructField(\"Mitoses\", DoubleType(), True),\n",
    "    StructField(\"Class\", DoubleType(), True)\n",
    "])\n",
    "cancer_df = spark.read.csv(\"/home/training/data/breast-cancer-wisconsin.csv\", schema=schema)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.sql.types import *\n",
    "\n",
    "schema = StructType( [\n",
    "    StructField(\"Sample Code Number\", StringType(), True),\n",
    "    StructField(\"Clump Thickness\", DoubleType(), True),\n",
    "    StructField(\"Uniformity of Cell Size\", DoubleType(), True),\n",
    "    StructField(\"Uniformity of Cell Shape\", DoubleType(), True),\n",
    "    StructField(\"Marginal Adhesion\", DoubleType(), True),\n",
    "    StructField(\"Single Epithelial Cell Size\", DoubleType(), True),\n",
    "    StructField(\"Bare Nuclei\", DoubleType(), True),\n",
    "    StructField(\"Bland Chromatin\", DoubleType(), True),\n",
    "    StructField(\"Normal Nucleoli\", DoubleType(), True),\n",
    "    StructField(\"Mitoses\", DoubleType(), True),\n",
    "    StructField(\"Class\", DoubleType(), True)\n",
    "])\n",
    "cancer_df = spark.read.csv(\"/home/training/data/breast-cancer-wisconsin.csv\", schema=schema)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us do some cleaning up. Out of the 699 records, there are 16 records that are missing some attribute values. We will remove these 16 records and process the rest (in total, 683 records). Since the first column in the dataset only contains the record's ID number, it is better to remove this column from subsequent calculations. Lastly, since the dataset's classifier is either benign cases (last column value = 2) or malignant cases (last column value = 4), we shall convert all benign cases from 2 to 0 and all malignant case from 4 to 1, which will make the later calculations much easier. The code is as follows:\n",
    "```python\n",
    "cancer_clean_df = cancer_df.drop('Sample Code Number').dropna().replace([2.0, 4.0], [0.0, 1.0], 'Class')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "cancer_clean_df = cancer_df.drop('Sample Code Number').dropna().replace([2.0, 4.0], [0.0, 1.0], 'Class')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Splitting the dataset\n",
    "We shall split the whole dataset into training data (70%) and test data (30%) randomly. Please note that the random split will generate around 211 test records. It is approximately but NOT exactly 30% of the dataset. You can place any value for the `seed` parameter:\n",
    "```python\n",
    "training_data, test_data = cancer_clean_df.randomSplit([0.7, 0.3], seed=1)\n",
    "training_data.persist()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "DataFrame[Clump Thickness: double, Uniformity of Cell Size: double, Uniformity of Cell Shape: double, Marginal Adhesion: double, Single Epithelial Cell Size: double, Bare Nuclei: double, Bland Chromatin: double, Normal Nucleoli: double, Mitoses: double, Class: double]"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "training_data, test_data = cancer_clean_df.randomSplit([0.7, 0.3], seed=1)\n",
    "training_data.persist()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating Transformers\n",
    "Before we can use the dataset to estimate a model, we need to do some transformations. So, let us import them all:\n",
    "```python\n",
    "import pyspark.ml.feature as ft\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.feature as ft"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Spark machine learning algorithms work with two columns that must be named `features` and `label`, by default. The `features` column must be a _vector_ representation of the features we intend to use to estimate a model while the `label` column represents the column with the different outcomes. In our exercise, our `label` column is `Class` which stores the outcome of the classification (either benign or malignant).\n",
    "\n",
    "To create the `features` column, we need to use an important transformer called `VectorAssembler` that collates multiple numeric columns into a single column with a vector representation. For example, if you have three columns in your DataFrame:\n",
    "```python\n",
    "df = spark.createDataFrame( [(12, 10, 3), (1, 4, 2)], ['a', 'b', 'c'])\n",
    "```\n",
    "The output of calling:\n",
    "```python\n",
    "ft.VectorAssembler(inputCols=['a', 'b', 'c'], outputCol='features').transform(df).collect()\n",
    "```\n",
    "would be:\n",
    "```\n",
    "[Row(a=12, b=10, c=3, features=DenseVector([12.0, 10.0, 3.0])),\n",
    " Row(a=1, b=4, c=2, features=DenseVector([1.0, 4.0, 2.0]))]\n",
    "```\n",
    "Notice that the new column `features` is a vector representation of the columns a, b and c.\n",
    "\n",
    "Similarly, we shall use the `VectorAssembler` now to generate our `features` column for our dataset as follows:\n",
    "```python\n",
    "input_cols = [e for e in cancer_clean_df.columns if e != 'Class']\n",
    "featuresCreator = ft.VectorAssembler(inputCols=input_cols, outputCol=\"features\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "input_cols = [e for e in cancer_clean_df.columns if e != 'Class']\n",
    "featuresCreator = ft.VectorAssembler(inputCols=input_cols, outputCol=\"features\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating an Estimator\n",
    "We shall now create our estimator, the Decision Tree classifer:\n",
    "```python\n",
    "import pyspark.ml.classification as cl\n",
    "dt = cl.DecisionTreeClassifier(labelCol=\"Class\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.classification as cl\n",
    "dt = cl.DecisionTreeClassifier(labelCol=\"Class\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating a Pipeline\n",
    "Now, create a Pipeline to pull the different transformations together:\n",
    "```python\n",
    "from pyspark.ml import Pipeline\n",
    "pipeline = Pipeline( stages = [featuresCreator, dt] )\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml import Pipeline\n",
    "pipeline = Pipeline( stages = [featuresCreator, dt] )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fitting the Model\n",
    "Next, we fit our model:\n",
    "```python\n",
    "model = pipeline.fit(training_data)\n",
    "```\n",
    "The `fit` method of the `pipeline` object takes our training dataset as an input. Under the hood, the `training_data` dataset is passed to the `featuresCreator` that creates the `features` column. Then, the output from this stage is passed to the decision tree classifier object that creates the final model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = pipeline.fit(training_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluating the Model\n",
    "Obviously, we would like to know how good is our model. The `fit` method returns the PipelineModel object (the model object in the preceding cell) that can then be used for prediction. We do the predictions by calling the `transform` method and pass the testing dataset we created earlier.\n",
    "```python\n",
    "predictions = model.transform(test_data)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "predictions = model.transform(test_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PySpark exposes a number of evaluation methods for classification and regression in the `evaluation` section of the package:\n",
    "```python\n",
    "import pyspark.ml.evaluation as ev\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.evaluation as ev"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use the `MulticlassClassificationEvaluator` to test how well our model performed:\n",
    "```python\n",
    "evaluator = ev.MulticlassClassificationEvaluator(predictionCol=\"prediction\", labelCol=\"Class\", \n",
    "                                                 metricName=\"accuracy\")\n",
    "accuracy = evaluator.evaluate(predictions)\n",
    "print(accuracy)\n",
    "```\n",
    "The accuracy is about 94% which is considered relatively good."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.9455445544554455\n"
     ]
    }
   ],
   "source": [
    "evaluator = ev.MulticlassClassificationEvaluator(predictionCol=\"prediction\", labelCol=\"Class\", metricName=\"accuracy\")\n",
    "accuracy = evaluator.evaluate(predictions)\n",
    "print(accuracy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To view the structure of the tree, we need to first extract the decision tree which is the second component (index = 1) in the pipeline and then use the `toDebugString` method:\n",
    "```python\n",
    "dtree = model.stages[1]\n",
    "print(dtree.toDebugString)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "DecisionTreeClassificationModel (uid=DecisionTreeClassifier_440588c01cf3b8ac6a8e) of depth 5 with 29 nodes\n",
      "  If (feature 1 <= 2.5)\n",
      "   If (feature 0 <= 7.5)\n",
      "    If (feature 5 <= 4.5)\n",
      "     Predict: 0.0\n",
      "    Else (feature 5 > 4.5)\n",
      "     If (feature 0 <= 3.5)\n",
      "      Predict: 0.0\n",
      "     Else (feature 0 > 3.5)\n",
      "      If (feature 3 <= 4.5)\n",
      "       Predict: 1.0\n",
      "      Else (feature 3 > 4.5)\n",
      "       Predict: 0.0\n",
      "   Else (feature 0 > 7.5)\n",
      "    Predict: 1.0\n",
      "  Else (feature 1 > 2.5)\n",
      "   If (feature 2 <= 2.5)\n",
      "    If (feature 6 <= 3.5)\n",
      "     Predict: 0.0\n",
      "    Else (feature 6 > 3.5)\n",
      "     If (feature 0 <= 1.5)\n",
      "      Predict: 0.0\n",
      "     Else (feature 0 > 1.5)\n",
      "      Predict: 1.0\n",
      "   Else (feature 2 > 2.5)\n",
      "    If (feature 1 <= 4.5)\n",
      "     If (feature 5 <= 5.5)\n",
      "      If (feature 3 <= 3.5)\n",
      "       Predict: 0.0\n",
      "      Else (feature 3 > 3.5)\n",
      "       Predict: 1.0\n",
      "     Else (feature 5 > 5.5)\n",
      "      If (feature 8 <= 2.5)\n",
      "       Predict: 1.0\n",
      "      Else (feature 8 > 2.5)\n",
      "       Predict: 1.0\n",
      "    Else (feature 1 > 4.5)\n",
      "     If (feature 7 <= 2.5)\n",
      "      If (feature 7 <= 1.5)\n",
      "       Predict: 1.0\n",
      "      Else (feature 7 > 1.5)\n",
      "       Predict: 1.0\n",
      "     Else (feature 7 > 2.5)\n",
      "      Predict: 1.0\n",
      "\n"
     ]
    }
   ],
   "source": [
    "dtree = model.stages[1]\n",
    "print(dtree.toDebugString)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Saving the Model\n",
    "PySpark allows you to save the Pipeline definition for later use. It not only saves the pipeline structure, but also all the definitions of all the Transformers and Estimators:\n",
    "```python\n",
    "pipeline.write().overwrite().save(\"./cancer_pipeline\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "pipeline.write().overwrite().save(\"./cancer_pipeline\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can load it up later and use it straight away to `fit` and predict:\n",
    "```python\n",
    "loadedPipeline = Pipeline.load(\"./cancer_pipeline\")\n",
    "loadedPipeline.fit(training_data).transform(test_data).take(1)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[Row(Clump Thickness=1.0, Uniformity of Cell Size=1.0, Uniformity of Cell Shape=1.0, Marginal Adhesion=1.0, Single Epithelial Cell Size=1.0, Bare Nuclei=1.0, Bland Chromatin=1.0, Normal Nucleoli=3.0, Mitoses=1.0, Class=0.0, features=DenseVector([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 3.0, 1.0]), rawPrediction=DenseVector([282.0, 0.0]), probability=DenseVector([1.0, 0.0]), prediction=0.0)]"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "loadedPipeline = Pipeline.load(\"./cancer_pipeline\")\n",
    "loadedPipeline.fit(training_data).transform(test_data).take(1)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
