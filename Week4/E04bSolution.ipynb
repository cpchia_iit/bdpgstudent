{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## E-Learning 4b - Linear Regression (Solution)\n",
    "Linear regression has been one of the most widely used regression methods and one of the basic analytical methods in statistics, and it is still widely used today. That is because modeling linear relationships is much easier than modeling nonlinear ones. Interpretation of the resulting models is also easier. The theory behind linear regression also forms the basis for more advanced methods and algorithms in machine learning. Like other types of regression, linear regression let you use a set of independent variables to make predictions about a target variable and quantify the relationship between them. Linear regression makes an assumption that there is a linear relationship (hence the name) between the independent and target variables.\n",
    "\n",
    "In this exercise, we shall use another commonly used dataset for machine learning - a housing dataset from the UCI machine learning repository. The file is `housing.csv` and is located in the `data` directory. The dataset contains 14 columns with the first 13 columns being the features that determine the median price (the last column) of an owner-occupied house in Boston, USA. We have chosen and cleaned the first eight columns as features. We shall use the first 200 rows to train and predict the median price. The first eight features (separated by commas) are as follows:\n",
    "\n",
    "| Feature | Description |\n",
    "|:--------| :-----------|\n",
    "| CRIM    | Per capita crime rate by town |\n",
    "| ZN      | Proportion of residential land zoned for lots over 25,000 square feet |\n",
    "| INDUS   | Proportion of non-retail business acres per town |\n",
    "| CHAS    | Charles River dummy variable (= 1 if tract bounds river; 0 otherwise) |\n",
    "| NOX     | Nitric oxide concentration (parts per 10 million) |\n",
    "| RM      | Average number of rooms per dwelling |\n",
    "| AGE     | Proportion of owner-occupied units built prior to 1940 |\n",
    "| DIS     | Weighted distances to five Boston employment centres |\n",
    "\n",
    "### Loading the Data\n",
    "First, let us load the dataset into a DataFrame:\n",
    "```python\n",
    "house_df = spark.read.csv('/home/training/data/housing.csv', inferSchema=True)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "house_df = spark.read.csv('/home/training/data/housing.csv', inferSchema=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating transformers\n",
    "Since we are using the first eight features, we shall assemble them into a vector using the `VectorAssembler`.\n",
    "```python\n",
    "import pyspark.ml.feature as ft\n",
    "input_columns = house_df.columns[:8]\n",
    "featuresCreator = ft.VectorAssembler(inputCols=input_columns, outputCol='features')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.feature as ft\n",
    "input_columns = house_df.columns[:8]\n",
    "featuresCreator = ft.VectorAssembler(inputCols=input_columns, outputCol='features')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating an Estimator\n",
    "Next, we shall create our linear regression model. \n",
    "```python\n",
    "import pyspark.ml.regression as reg\n",
    "last_column = house_df.columns[-1]\n",
    "lr = reg.GeneralizedLinearRegression(labelCol=last_column)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.regression as reg\n",
    "last_column = house_df.columns[-1]\n",
    "lr = reg.GeneralizedLinearRegression(labelCol=last_column)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating a Pipeline\n",
    "Now, create a Pipeline to pull the different transformations together:\n",
    "```python\n",
    "from pyspark.ml import Pipeline\n",
    "pipeline = Pipeline( stages = [featuresCreator, lr] )\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml import Pipeline\n",
    "pipeline = Pipeline( stages = [featuresCreator, lr] )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the pipeline and estimate our model. We shall use the whole dataset for training:\n",
    "```python\n",
    "model = pipeline.fit(house_df)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = pipeline.fit(house_df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluating the Performance of the Model\n",
    "Since we have created our linear regression model, let us use our original data (`house_df`) to see how well our model does its predictions:\n",
    "```python\n",
    "predictions = model.transform(house_df)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "predictions = model.transform(house_df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PySpark exposes a number of evaluation methods for classification and regression in the `evaluation` section of the package. We will use the `RegressionEvaluator` to test how well our model performed:\n",
    "```python\n",
    "import pyspark.ml.evaluation as ev\n",
    "evaluator = ev.RegressionEvaluator(predictionCol=\"prediction\", labelCol=last_column)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyspark.ml.evaluation as ev\n",
    "evaluator = ev.RegressionEvaluator(predictionCol=\"prediction\", labelCol=last_column)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us see how well our model performed:\n",
    "```python\n",
    "print(evaluator.evaluate(predictions, {evaluator.metricName: 'rmse'}))\n",
    "print(evaluator.evaluate(predictions, {evaluator.metricName: 'r2'}))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3.6890338365263275\n",
      "0.781780503247152\n"
     ]
    }
   ],
   "source": [
    "print(evaluator.evaluate(predictions, {evaluator.metricName: 'rmse'}))\n",
    "print(evaluator.evaluate(predictions, {evaluator.metricName: 'r2'}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**RMSE** is defined as the sum of the squares of the difference between the real values and the predicted values:\n",
    "\\begin{equation}\n",
    "RMSE = \\sqrt{ \\frac{1}{N} \\sum_{i=0}^{N} (\\hat{y} - y)^2 }\n",
    "\\end{equation}\n",
    "where $\\hat{y}$ are the predicted values, and $y$ are the real values we want to predict. The closer the predictions are to the real values, the lower the RMSE is. A lower RMSE therefore means a better, more accurate prediction.\n",
    "\n",
    "On the other hand, `r2` (called **r-squared**) is known as the coefficient of determination and is the fraction of the total variation that is captured by your model. So how well does your regression line follow that variation that is happening? Are we getting an equal amount of variance on either side of your line or not? That is what r-squared is measuring. To actually compute the value, take 1 minus the sum of the squared errors over the sum of the squared variations from the mean:\n",
    "\\begin{equation}\n",
    "1.0 - \\frac{sum\\,of\\,squared\\,errors}{sum\\,of\\,squared\\,variations\\,from\\,mean}\n",
    "\\end{equation}\n",
    "\n",
    "For r-squared, you will get a value that ranges from zero to one. 0 means your fit is terrible. It does not capture any of the variance in your data. 1 is a perfect fit, where all of the variance in your data gets captured by this line. Hence, a low r-squared value means it is a poor fit, a high r-squared value means it is a good fit."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using the Predictive Model\n",
    "You can use the predictive model to predict the median price of a house. The following gives an example of how this can be done.\n",
    "```python\n",
    "test_record = [ ( 0.02735, 0.0, 7.01, 0, 0.469, 6.431, 77.9, 4.9672 ) ]\n",
    "df = spark.createDataFrame(test_record, schema=input_columns)\n",
    "model.transform(df).first()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Row(_c0=0.02735, _c1=0.0, _c2=7.01, _c3=0, _c4=0.469, _c5=6.431, _c6=77.9, _c7=4.9672, features=DenseVector([0.0274, 0.0, 7.01, 0.0, 0.469, 6.431, 77.9, 4.9672]), prediction=23.26400429533625)"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "test_record = [ ( 0.02735, 0.0, 7.01, 0, 0.469, 6.431, 77.9, 4.9672 ) ]\n",
    "df = spark.createDataFrame(test_record, schema=input_columns)\n",
    "model.transform(df).first()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
