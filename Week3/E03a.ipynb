{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## E-Learning 3a - Using Spark SQL for Data Preparation\n",
    "The data preparation phase is an extremely important phase, not only for the data processing to work properly, but also for you to develop a better understanding of your data so that you can take the right approach when implementing a solution. The main task is to consolidate all the data and then clean, format, and transform the data to the format needed for your analysis.\n",
    "\n",
    "In this exercise, we will practise some commonly used techniques in data preparation using Spark SQL through a couple of examples.\n",
    "\n",
    "### Duplicates\n",
    "First, create a new DataFrame by running the following cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_dup = spark.createDataFrame([\n",
    "    (1, 144.5, 5.9, 33, 'M'),\n",
    "    (2, 167.2, 5.4, 45, 'Male'),\n",
    "    (3, 124.1, 5.2, 23, 'F'),\n",
    "    (4, 144.5, 5.9, 33, 'M'),\n",
    "    (5, 133.2, 5.7, 54, 'F'),\n",
    "    (3, 124.1, 5.2, 23, 'F'),\n",
    "    (5, 129.2, 5.3, 42, 'M'), ], ['id', 'weight', 'height', 'age', 'gender'])\n",
    "df_dup.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that there are two rows with id equals to 3 and they have exactly the same content. In other words, they are duplicates of each other. If your data is large, you will not be able to manually look for duplicates. What you can do is to compare the counts of the full dataset with the one you get after using the `distinct` method. For example, try the following:\n",
    "```python\n",
    "print(\"Count of rows: {0}\".format(df_dup.count()))\n",
    "print(\"Count of distinct rows: {0}\".format(df_dup.distinct().count()))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We remove duplicates (records that are exact copies of each other) using the `dropDuplicates` method:\n",
    "```python\n",
    "df_dup1 = df_dup.dropDuplicates()\n",
    "df_dup1.show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the resulting DataFrame, notice that rows with id 1 and 4 have exactly the same content except their ids. We can safely assume they are the same person and can remove one of them as follows:\n",
    "```python\n",
    "df_dup2 = df_dup1.dropDuplicates(subset=[\"weight\", \"height\", \"age\", \"gender\"])\n",
    "df_dup2.show()\n",
    "```\n",
    "The `subset` parameter indicates that records with the same weight, height, age and gender should be dropped."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the resulting DataFrame, notice that there are two ids equal to 5 but the content of these two records are different. These are probably two different person but one of the ids is entered wrongly. In a large dataset, you can do the following to check whether there are any duplicated ids:\n",
    "```python\n",
    "import pyspark.sql.functions as fn\n",
    "df_dup2.agg( fn.count('id').alias('count'), fn.countDistinct('id').alias('distinct') ).show()\n",
    "```\n",
    "The `agg` method enables the calculation of the total and distinct number of ids to be done in one step while the `alias` method allows us to specify a friendly name to each returned column."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead of changing the duplicated id, we can use the `monotonically_increasing_id` method to give each record a unique and increasing id as follows:\n",
    "```python\n",
    "df_dup2.withColumn('new_id', fn.monotonically_increasing_id()).show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us display the number of distinct values of gender:\n",
    "```python\n",
    "df_dup2.groupby(\"gender\").count().show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the gender is typically 'F' or 'M' but there is one record with gender \"Male\". It would be good to standardise the values in the `gender` column by replacing occurences of \"Male\" with \"M\". We can use the `replace` method to do this:\n",
    "```python\n",
    "final_df = df_dup2.replace(\"Male\", \"M\", \"gender\")\n",
    "final_df.show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to replace more than one value e.g. \"F\" to \"Female\" and \"M\" to \"Male\", you can do the following:\n",
    "```python\n",
    "final_df.replace([\"F\", \"M\"], [\"Female\", \"Male\"]).show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Missing Observations\n",
    "Now, let us look at how to deal with data with missing values in one or more fields among its records. The missing values could be due to system failure, people error, data schema changes etc. Let us create another DataFrame by running the next cell (the `None` represents missing values and they will be showed as `null` in the DataFrame)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_miss = spark.createDataFrame([\n",
    "    (1, 143.5, 5.6, 28,   'M',  100000),\n",
    "    (2, 167.2, 5.4, 45,   'M',  None),\n",
    "    (3, None , 5.2, None, None, None),\n",
    "    (4, 144.5, 5.9, 33,   'M',  None),\n",
    "    (5, 133.2, 5.7, 54,   'F',  None),\n",
    "    (6, 124.1, 5.2, None, 'F',  None),\n",
    "    (7, 129.2, 5.3, 42,   'M',  76000), ], ['id', 'weight', 'height', 'age', 'gender', 'income'])\n",
    "df_miss.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let us check what percentage of missing values are there in each column as follows:\n",
    "```python\n",
    "df_miss.agg( * [(1 - fn.count(c) / fn.count(\"*\")).alias(c + '_missing') for c in df_miss.columns] ).show()\n",
    "```\n",
    "The `fn.count(c)` counts the number of values in column c **excluding `None` values** while `fn.count(\"*\")` counts all rows in column c **including those which contain the `None` value**. On the other hand, the * preceding the list declaration instructs the `agg` method to treat the list as a set of parameters passed to the function `agg` instead of treating the list as a single parameter to the `agg` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since the income field has the highest percentage of missing values, let us remove it as follows:\n",
    "```python\n",
    "df_miss_no_income = df_miss.drop(\"income\")\n",
    "df_miss_no_income.show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One way to deal with records with missing values is to remove these records if there are not many of them. We can do this with the `dropna` method as follows:\n",
    "```python\n",
    "df_miss_no_income.dropna(subset=[\"weight\", \"height\", \"age\", \"gender\"]).show()\n",
    "```\n",
    "Any records having a missing value in _any_ of the columns specified in `subset` would be removed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another way is to _impute_ the missing values using the `fillna` method. Assume that we want to replace any missing values in the weight column with the mean weight, we shall first compute the mean weight using the `mean` method and then use the `fillna` method to replace the missing values:\n",
    "```python\n",
    "mean_wt = df_miss_no_income.agg(fn.mean(df_miss_no_income.weight)).first()[0]\n",
    "df_miss_no_income.fillna(mean_wt, \"weight\").show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also fill missing values with some _default_ value e.g. let us replace any missing values in `gender` with the word \"missing\" as follows:\n",
    "```python\n",
    "df_miss_no_income.fillna(\"missing\", \"gender\").show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Outliers\n",
    "Outliers are those observations that deviate significantly from the distribution of the rest of your data. The definition of _significance_ varies but in the most general form, you can accept that there are no outliers if all the values are roughly within the Q1-1.5IQR and Q3+1.5IQR range, where IQR is the interquartile range which is the difference between the 75th percentile (the Q3) and 25th percentile (the Q1).\n",
    "\n",
    "Let us practice dealing with outliers by first running the following cell to create a new DataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_outliers = spark.createDataFrame([        \n",
    "    (1, 143.5, 5.3, 28),\n",
    "    (2, 154.2, 5.5, 45),\n",
    "    (3, 342.3, 5.1, 99),\n",
    "    (4, 144.5, 5.5, 33),\n",
    "    (5, 133.2, 5.4, 54),\n",
    "    (6, 124.1, 5.1, 21),\n",
    "    (7, 129.2, 5.3, 42),], ['id', 'weight', 'height', 'age'])\n",
    "df_outliers.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us find the outliers for the weight column. First, we calculate the upper and lower cut off points for the weight column using the `approxQuantile` method.\n",
    "```python\n",
    "quantiles = df_outliers.approxQuantile(\"weight\", [0.25, 0.75], 0.05)\n",
    "IQR = quantiles[1] - quantiles[0]\n",
    "bounds = [ quantiles[0] - 1.5 * IQR, quantiles[1] + 1.5 * IQR ]\n",
    "df_outliers.filter( (df_outliers.weight < bounds[0]) | (df_outliers.weight > bounds[1]) ).show()\n",
    "```\n",
    "The first parameter to `approxQuantile` is the name of the column, the second parameter can be either a number between 0 or 1 (where 0.5 means to calculate median) or a list (as in our case, to calculate the 25th and 75th percentile), and the third parameter specifies the acceptable level of an error for each column (if set to 0, it will calculate the exact quantile values for the column, but it can be really expensive to do so for a large dataset). The `bounds` will hold the lower and upper bounds for the weight column."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
