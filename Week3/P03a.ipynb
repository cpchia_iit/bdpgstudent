{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Practical 3a - Using Spark SQL for Data Preparation\n",
    "Any serious data scientist or data modeler must be acquainted with the dataset before starting any modeling. In this exercise, we shall practice doing some data exploration. We shall use the dataset `ccFraud.csv.gz` which is a simulated dataset on credit card fraud. The data is approximately 200MB and contains the following fields (each separated by a comma):\n",
    "\n",
    "```\n",
    "custID: A unique identifier for each customer\n",
    "gender: Gender of the customer\n",
    "state: State in the United States where the customer lives\n",
    "cardholder: Number of credit cards the customer holds\n",
    "balance: Balance on the credit card\n",
    "numTrans: Number of transactions to date\n",
    "numIntlTrans: Number of international transactions to date\n",
    "creditLine: The financial services corporation, such as Visa, MasterCard, and American Express\n",
    "fraudRisk: Binary variable, 1 means customer being frauded, 0 means otherwise\n",
    "```\n",
    "The following are the first 10 lines of the file:\n",
    "```\n",
    "\"custID\",\"gender\",\"state\",\"cardholder\",\"balance\",\"numTrans\",\"numIntlTrans\",\"creditLine\",\"fraudRisk\"\n",
    "1,1,35,1,3000,4,14,2,0\n",
    "2,2,2,1,0,9,0,18,0\n",
    "3,2,2,1,0,27,9,16,0\n",
    "4,1,15,1,0,12,0,5,0\n",
    "5,1,46,1,0,11,16,7,0\n",
    "6,2,44,2,5546,21,0,13,0\n",
    "7,1,3,1,2000,41,0,1,0\n",
    "8,1,10,1,6016,20,3,6,0\n",
    "9,2,32,1,2428,4,10,22,0\n",
    "```\n",
    "Let us start by loading the dataset:\n",
    "```python\n",
    "fraudDF = spark.read.csv(\"/home/training/data/ccFraud.csv.gz\", inferSchema=True, header=True)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Descriptive Statistics\n",
    "For a better understanding of the categorical columns, we will count the frequencies of their values using the `groupby` method. For example, count the frequencies of the `gender` column as follows:\n",
    "```python\n",
    "fraudDF.groupby('gender').count().show()\n",
    "```\n",
    "Notice that the gender is fairly imbalanced. What you would expect to see is an equal distribution for both genders."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the truly numerical features, we can use the `describe` method:\n",
    "```python\n",
    "numerical = ['balance', 'numTrans', 'numIntlTrans']\n",
    "desc = fraudDF.describe(numerical)\n",
    "desc.show()\n",
    "```\n",
    "Notice that:\n",
    "- All the features are positively skewed. The maximum values are a number of times larger than the mean.\n",
    "- The coefficient of variation (the ratio of mean to standard deviation) is very high (close or greater than 1), suggesting a wide spread of observations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us find the skewness of the `balance` feature:\n",
    "```python\n",
    "fraudDF.agg( {'balance': 'skewness'} ).show()\n",
    "```\n",
    "A skewness of 0 means that the data are perfectly symmetrical."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Correlations\n",
    "Calculating correlations is very easy once your data is in a DataFrame. However, the `corr` method supports only the Pearson correlation coefficient at the moment and hence, it can only calculate pairwise correlations. Nonetheless, it is still useful for data exploration. Let us create a correlations matrix using the three numerical attributes as follows (it might take some time to compute the correlation matrix due to the large amount of data):\n",
    "```python\n",
    "n_numerical = len(numerical)\n",
    "corr = []\n",
    "\n",
    "for i in range(0, n_numerical):\n",
    "    temp = [None] * i\n",
    "    for j in range(i, n_numerical):\n",
    "        temp.append(fraudDF.corr(numerical[i], numerical[j]))\n",
    "    corr.append(temp)\n",
    "\n",
    "# print the correlation matrix in a nicely formatted table\n",
    "from tabulate import tabulate\n",
    "print(tabulate(corr, headers=numerical, showindex=numerical, tablefmt=\"fancy_grid\", numalign=\"center\"))\n",
    "```\n",
    "Notice that the correlations between the numerical features in the credit card fraud dataset are pretty non-existent. Thus, all these features can be used in our models."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data Visualization\n",
    "Since data visualization is already covered in an earlier subject, we shall not be spending too much time on this topic in this subject. You are free to use any tools e.g. Excel, QlikView, Power BI etc to do your data visualizations. For this exercise, we shall show you how you can use the `matplotlib` library to create a simple histogram programmatically using Python. If you are interested in generating those visualizations covered in the lecture programmatically using Python, please refer to the examples we provided in P03c.\n",
    "\n",
    "Start by loading the relevant modules and setting them up as follows:\n",
    "```\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "plt.style.use('ggplot')\n",
    "```\n",
    "The `%matplotlib inline` will make every chart generated with `matplotlib` appears within the notebook and not as a separate window."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us create a histogram of the `balance` column as follows:\n",
    "```\n",
    "bal_list = [i[0] for i in fraudDF.select(\"balance\").collect()]\n",
    "plt.title(\"Histogram of 'balance'\")\n",
    "plt.hist(bal_list, bins = 20)\n",
    "plt.show()\n",
    "```\n",
    "First, we collect all the `balance` values into a single list. Then, we set the title of the histogram and called the `hist` method to create a histogram with 20 bins. The `show` method displays the histogram graphically in the notebook. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
