{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Practical 1b - Web Scraping using Python\n",
    "Web scraping is the practice of gathering data through any means other than a program interacting with an API (or, obviously, through a human using a web browser). This is most commonly accomplished by writing an automated program that queries a web server, requests data (usually in the form of HTML and other files that comprise web pages), and then parses that data to extract needed information.\n",
    "\n",
    "Why do we want to do web scraping?\n",
    "- Save time – do not need to view pages one at a time to gather information\n",
    "- Can go to places traditional search engines cannot\n",
    "- There is no API provided by the website owner and even if the API exists, it might not serve our purpose\n",
    "\n",
    "Is web scraping legal?\n",
    "Well, it is not so clear at this point in time whether web scraping is legal. If the scraped data is being used for personal use, in practice, there is no problem. However, if the data is going to be republished, then the type of data scraped is important. When the scraped data constitutes facts (such as business locations and telephone listings), it can be republished. However, if the data is original (such as opinions and reviews), it most likely cannot be republished for copyright reasons.\n",
    "___"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Through this exercise, you will learn how to scrape a web page using Python. We shall be scraping from the website [www.camemberu.com](http://www.camemberu.com). We shall write a Python program to extract information from one of its blog entries. Specifically, we will extract the title and date, list of labels as well as calculate the number of images in that blog. Take a look at this [blog entry](http://www.camemberu.com/2017/04/mcdonalds-buttermilk-crispy-chicken.html). You may want to `right-click -> View Page Source` to examine the underlying HTML content."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python contains many useful libraries called **modules** which we can `import` to use. In this exercise, we shall import the [requests](http://docs.python-requests.org/en/master/) and the \n",
    "[BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) modules.\n",
    "```python\n",
    "import requests\n",
    "from bs4 import BeautifulSoup\n",
    "```\n",
    "The `requests` module contains functions for requesting data across the web while `BeautifulSoup` is a popular module that parses a web page and then provides a convenient interface to navigate content."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let us retrieve the web page and start parsing with `BeautifulSoup`:\n",
    "```python\n",
    "blog = requests.get(\"http://www.camemberu.com/2017/04/mcdonalds-buttermilk-crispy-chicken.html\")\n",
    "bs = BeautifulSoup(blog.content, 'lxml')\n",
    "```\n",
    "The `get` method is used for downloading the webpage from the website and we use the downloaded content (`blog.content`) to create a `BeautifulSoup` object. The `BeautifulSoup` object uses the specify parser (`lxml`) to analyse the HTML content. Internally, `BeautifulSoup` constructs a tree structure of the HTML content e.g. each HTML tag such as h1, h2, div, ul etc becomes a node in this tree. `BeautifulSoup` uses this tree structure to search for information contained in the web page. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Examining the source of the webpage, you will notice that the title is encapsulated in a `h3` tag as follows (line 1231):\n",
    "```html\n",
    "<h3 class='post-title entry-title' itemprop='headline'>\n",
    "McDonald's Buttermilk Crispy Chicken Burger\n",
    "</h3>\n",
    "```\n",
    "Note that the tag has an attribute call `class`. We shall use this attribute to find the `h3` node containing the title in the BeautifulSoup object:\n",
    "```python\n",
    "title = bs.find(attrs={'class' : \"post-title entry-title\"})\n",
    "print(title.string)\n",
    "```\n",
    "The `find` method finds the first occurrence of the tag with attribute class `post-title entry-title` while `title.string` refers to the text encapsulated by the `h3` tag which is also the title of the blog entry we are looking for."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us examine the page's source again to find the date. You will notice that the date is encapsulated in a `h2` tag (line 1220) as follows:\n",
    "```html\n",
    "<h2 class='date-header'><span>Thursday, April 13, 2017</span></h2>\n",
    "```\n",
    "Similarly, the tag has an attribute `class` which we can use to find the `h2` node containing the date:\n",
    "```python\n",
    "date = bs.find(attrs={'class' : 'date-header'})\n",
    "print(date.string)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let us look for the labels in the page's source. The labels are encapsulated in a `span` tag (line 1305) as follows:\n",
    "```html\n",
    "<span class='post-labels'>\n",
    "Labels:\n",
    "<a href='http://www.camemberu.com/search/label/%2B%20Singapore%20%28islandwide%29' rel='tag'>+ Singapore (islandwide)</a>,\n",
    "<a href='http://www.camemberu.com/search/label/burgers' rel='tag'>burgers</a>,\n",
    "<a href='http://www.camemberu.com/search/label/chicken' rel='tag'>chicken</a>,\n",
    "<a href='http://www.camemberu.com/search/label/fast%20food' rel='tag'>fast food</a>,\n",
    "<a href='http://www.camemberu.com/search/label/McDonalds' rel='tag'>McDonalds</a>\n",
    "</span>\n",
    "```\n",
    "Notice that each label is encapsulated in an `a` tag. Hence, to retrieve the labels, we need to use the `class` attribute to find the correct `span` tag first and from the `span` tag, we need to iterate through each `a` tag to get the labels.\n",
    "```python\n",
    "node = bs.find(attrs={'class' : \"post-labels\"})\n",
    "labels = node.find_all('a')\n",
    "for label in labels:\n",
    "    print(label.string)\n",
    "```\n",
    "The `find_all` function is used to find all occurrences of the `a` nodes which are children of the `span` node with attribute `class post-label`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last piece of information is the number of images in the blog entry. Examine the page's source again. Notice that the images end with a .jpg extension and is encapsulated in a `div` tag (line 1237) with attribute class `post-body entry-content`. Hence, we shall use this `class` attribute to find the correct `div` tag and then find all the `img` tags and count them.\n",
    "```python\n",
    "post_body = bs.find(attrs={\"class\": \"post-body entry-content\"})\n",
    "images = post_body.find_all(\"img\")\n",
    "print(len(images))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***Caution***: When writing web scraping scripts, avoid using loops where possible when you are developing your program. This is to prevent the case where your loop has errors, causing the scraping to be done continuously on the website. This will get the attention of the web administrator who might then ban your ip from scraping the website. Once you are confident that your program is working correctly, you can start including looping so that you could iterate through the various urls."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
