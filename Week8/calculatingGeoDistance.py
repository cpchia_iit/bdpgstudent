import utilities.geoCalc as geo
from utilities.converters import metricImperial

from pyspark.sql import SparkSession
import pyspark.sql.functions as fn

def findDistance(lat1, long1, lat2, long2):
    return geo.calculateDistance( (lat1, long1), (lat2, long2) )

def convertMiles2Km(m):
    return metricImperial.convert(str(m) + ' mile', 'km')

if __name__ == '__main__':
    spark = SparkSession.builder.appName('CalculatingGeoDistances').getOrCreate()
    print('Session created')

    uber = spark.read.csv("/home/training/data/taxi_data.csv", header=True, inferSchema=True)

    getDistance = fn.udf(findDistance)
    convertMiles = fn.udf(convertMiles2Km)

    uber_dist_df = uber.withColumn('miles', getDistance(
        fn.col('pickup_latitude'),
        fn.col('pickup_longitude'), 
        fn.col('dropoff_latitude'), 
        fn.col('dropoff_longitude')
    ))

    uber_dist_df.withColumn("kilometers", convertMiles(fn.col('miles'))).show()

    spark.stop()

