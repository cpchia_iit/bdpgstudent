import sys
from pyspark.sql import SparkSession
import pyspark.sql.functions as fn

if __name__ == "__main__":
  if len(sys.argv) < 2:
    print >> sys.stderr, "Usage: WordCount <file>"
    exit(-1)


spark = SparkSession.builder.appName('WordCounting').getOrCreate()
print('Session created')

lines_df = spark.read.text(sys.argv[1])
words_df = lines_df.select(fn.split(lines_df.value, " ").alias("words"))
word_list = words_df.select(fn.explode(words_df.words).alias("word"))
counts = word_list.groupBy("word").count()
counts.show()

spark.stop()
