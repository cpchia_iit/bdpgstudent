import sys
from pyspark.sql import SparkSession
import pyspark.sql.functions as fn

if __name__ == "__main__":
  if len(sys.argv) < 2:
    print >> sys.stderr, "Usage: CountJPGs.py <file>"
    exit(-1)


spark = SparkSession.builder.appName('CountJPGs').getOrCreate()
print('Session created')

logs_df = spark.read.text(sys.argv[1])
print(logs_df.filter("value LIKE '%.jpg%'").count())

spark.stop()
