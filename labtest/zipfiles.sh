#!/bin/bash

char="_"

# first, check that files Q1.ipynb and Q2.ipynb exist
if [[ -e Q1.ipynb && -e Q2.ipynb ]]
then
  # get student number and name
  printf "Enter your student number e.g. 1781122H: "
  read num
  printf "Enter your name: "
  read name
  name="${name// /$char}"

  # zip Q1 and Q2
  zip ${num}_${name}.zip Q1.ipynb Q2.ipynb
  printf "Your zip file contains the following:\n"
  unzip -l ${num}_${name}.zip
  printf "Please upload ${num}_${name}.zip to Polymall\n"
else
  printf "Either Q1.ipynb or Q2.ipynb does not exist\n"
  exit 1
fi
