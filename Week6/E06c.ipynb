{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## E-Learning 6c - Using GraphFrames\n",
    "In this exercise, we are going to perform some graph analytics using GraphFrames. We shall be using the following two datasets (located in the `data` directory):\n",
    "1. _Airline On-Time Performance and Causes of Flight Delays_ (`departuredelays.csv`). This dataset contains details of flight delays as reported by US air carriers. The data is collected by the Office of Airline Information, Bureau of Transportation Statistics (BTS) between 1/1/2014 and 31/3/2014. There is a header line and five fields (each separated by comma):\n",
    "    * Date of flight: The format is MDDhhmm (M represents a month in year 2014 while mm represents minutes)\n",
    "    * delays: Difference in minutes between scheduled and actual departure time. Early departures show negative numbers.\n",
    "    * distance: Distance between airports in nautical miles\n",
    "    * origin: Origin Airport\n",
    "    * destination: Destination Airport\n",
    "    \n",
    "2. _Open Flights: Airports and airline data_ (`airport-codes-na.txt`). This dataset contains the list of US airport data. There is a header line and four fields (each separated by a tab):\n",
    "    * City: Main city served by airport (may be different from airport name)\n",
    "    * State: State code\n",
    "    * Country: Either USA or Canada\n",
    "    * IATA: 3-letter IATA code designating a specific airport, as defined by the International Air Transport Association\n",
    "\n",
    "### Preparing the flights dataset\n",
    "First, let us create the schema and then load the datasets into a DataFrame:\n",
    "```python\n",
    "airportsna = spark.read.csv(\"/home/training/data/airport-codes-na.txt\", header=True, inferSchema=True, sep='\\t')\n",
    "departureDelays = spark.read.csv(\"/home/training/data/departuredelays.csv\", header='true', inferSchema=True)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Second, let us create a list of distinct IATA codes from the `departureDelays` DataFrame. These airports appear in at least one trip in the departure delays dataset either as origin or destination.\n",
    "```python\n",
    "origin_iata = departureDelays.select(departureDelays.origin.alias(\"IATA\")).distinct()\n",
    "dest_iata = departureDelays.select(departureDelays.destination.alias(\"IATA\")).distinct()\n",
    "distinct_iata = origin_iata.union(dest_iata).distinct()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Third, from the list of IATA codes, create a DataFrame containing details of the airport (IATA, City, State, Country).\n",
    "```python\n",
    "airports = distinct_iata.join(airportsna, on=\"IATA\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Four, we rename the `date` column to be `tripid`. Then, we combine the airport information with the departure delays information to create a new DataFrame comprising of key attributes including date of flight, delays, distance, and airport information (origin, destination):\n",
    "```python\n",
    "df = departureDelays.withColumnRenamed(\"date\", \"tripid\")\n",
    "df2 = df.join(airports, df.origin == airports.IATA) \\\n",
    "    .withColumnRenamed(\"City\", \"city_src\").withColumnRenamed(\"State\", \"state_src\").drop(\"IATA\").drop(\"Country\")\n",
    "df3 = df2.join(airports, df2.destination == airports.IATA) \\\n",
    "    .withColumnRenamed(\"City\", \"city_dst\").withColumnRenamed(\"State\", \"state_dst\").drop(\"IATA\").drop(\"Country\")\n",
    "departureDelays_geo = df3.withColumnRenamed(\"origin\", \"src\").withColumnRenamed(\"destination\", \"dst\")\n",
    "departureDelays_geo.show(5)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Building the graph\n",
    "To build the graph using GraphFrames, we need to create two DataFrames. The first DataFrame contains information of the vertices and in our case, the vertices of our flight data are the airports. This DataFrame need to have a column called `id` which will represent the vertices. Hence, we shall rename the IATA airport code to `id` in our `airports` DataFrame:\n",
    "```python\n",
    "from graphframes import *\n",
    "tripVertices = airports.withColumnRenamed(\"IATA\", \"id\")\n",
    "tripVertices.show(5)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second DataFrame contains information of the edges and in our flight data, the edges are the flights. This DataFrame need to contain the columns `src` and `dst` whose content represents the origin and destination columns from the `departureDelays_geo` DataFrame:\n",
    "```python\n",
    "tripEdges = departureDelays_geo.select(\"tripid\", \"delay\", \"src\", \"dst\", \"city_dst\", \"state_dst\")\n",
    "tripEdges.show(5)\n",
    "```\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we create our GraphFrame using the vertices and edges DataFrames:\n",
    "```python\n",
    "tripGraph = GraphFrame(tripVertices, tripEdges)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Determining the number of airports and trips\n",
    "From the graph, determine the number of airports and trips. Expected output:\n",
    "```\n",
    "Airports: 279\n",
    "Trips: 1361141\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Determining the longest delay\n",
    "Find the longest delayed flight from the graph. Expected output:\n",
    "```\n",
    "+----------+\n",
    "|max(delay)|\n",
    "+----------+\n",
    "|      1642|\n",
    "+----------+\n",
    "```\n",
    "The longest delay is 1,642 minutes i.e. more than 27 hours!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Determining the number of delayed vs on-time/early flights\n",
    "Find the number of delayed versus on-time (or early) flights. Expected output:\n",
    "```\n",
    "On-time / Early Flights: 780469\n",
    "Delayed Flights: 580672\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What flights departing Seattle are most likely to have significant delays?\n",
    "Find the top five destinations for flights departing from Seattle that are most likely to have significant delays.  Start by determining the average delay for each destination from Seattle ('SEA') and then display the top five cities in descending order of average delay. Expected output:\n",
    "```\n",
    "+---+---+------------------+\n",
    "|src|dst|        avg(delay)|\n",
    "+---+---+------------------+\n",
    "|SEA|PHL|55.666666666666664|\n",
    "|SEA|COS| 43.53846153846154|\n",
    "|SEA|FAT| 43.03846153846154|\n",
    "|SEA|LGB| 39.39705882352941|\n",
    "|SEA|IAD|37.733333333333334|\n",
    "+---+---+------------------+\n",
    "only showing top 5 rows\n",
    "```\n",
    "Philadelphia (PHL), Colorado Springs (COS), Fresno (FAT), Long Beach (LGB), and Washington D.C (IAD) are the top five cities with flights delayed originating from Seattle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using vertex degrees\n",
    "The vertex degrees in our graph would represent flights to airports. Determine the top 10 busiest airports i.e. the top 10 vertices in terms of their degrees. Expected output:\n",
    "```\n",
    "+---+------+\n",
    "| id|degree|\n",
    "+---+------+\n",
    "|ATL|179774|\n",
    "|DFW|133966|\n",
    "|ORD|125405|\n",
    "|LAX|106853|\n",
    "|DEN|103699|\n",
    "|IAH| 85685|\n",
    "|PHX| 79672|\n",
    "|SFO| 77635|\n",
    "|LAS| 66101|\n",
    "|CLT| 56103|\n",
    "+---+------+\n",
    "only showing top 10 rows\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Determining the top transfer airports\n",
    "Many airports are used as transfer points instead of being the final destination. An easy way to calculate this is by calculating the value of `inDegrees` (the number of flights to the airport) / `outDegrees` (the number of flights leaving the airport). Values close to 1 may indicate many transfers, whereas values < 1 indicate many outgoing flights and values > 1 indicate many incoming flights. Find the top 10 transfer city airports (0.9 <= value <= 1.1). Expected output:\n",
    "```\n",
    "+---+--------------+------------------+\n",
    "| id|          city|       degreeRatio|\n",
    "+---+--------------+------------------+\n",
    "|MSP|   Minneapolis|0.9375183338222353|\n",
    "|DEN|        Denver| 0.958025717037065|\n",
    "|DFW|        Dallas| 0.964339653074092|\n",
    "|ORD|       Chicago|0.9671063983310065|\n",
    "|SLC|Salt Lake City|0.9827417906368358|\n",
    "|IAH|       Houston|0.9846895050147083|\n",
    "|PHX|       Phoenix|0.9891643572266746|\n",
    "|OGG| Kahului, Maui|0.9898718478710211|\n",
    "|HNL|Honolulu, Oahu| 0.990535889872173|\n",
    "|SFO| San Francisco|0.9909473252295224|\n",
    "+---+--------------+------------------+\n",
    "only showing top 10 rows\n",
    "```\n",
    "The results make sense since these airports are major hubs for national airlines. For example, Delta uses Minneapolis and Salt Lake City as its hub, Frontier uses Denver, American uses Dallas and Phoenix, United uses Houston, Chicago, and San Francisco, and Hawaiian Airlines uses Kahului and Honolulu as its hubs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using the Breadth-First Search\n",
    "The Breadth-first search (BFS) algorithm finds the shortest path from one set of vertices to another. Let us use BFS to traverse our `tripGraph` to quickly find the number of direct flights between Seattle and San Francisco.\n",
    "```python\n",
    "filteredPaths = tripGraph.bfs(fromExpr = \"id = 'SEA'\", toExpr = \"id = 'SFO'\",  maxPathLength = 1)\n",
    "filteredPaths.show(5)\n",
    "```\n",
    "`fromExpr` and `toExpr` are the expressions indicating the origin and destination airports (that is, SEA and SFO, respectively). The `maxPathLength = 1` indicates that we only want one edge between the two vertices, that is, a non-stop flight between Seattle and San Francisco. The results will show that there are many direct flights between Seattle and San Francisco."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now determine the number of direct flights between San Francisco and Buffalo. You will notice that there is no output i.e. there are no direct flights between these two cities. Instead, modify your query and change `maxPathLength` to 2 and you will be able to see more flight options. Expected output:\n",
    "```\n",
    "+--------------------+--------------------+-------------------+--------------------+--------------------+\n",
    "|                from|                  e0|                 v1|                  e1|                  to|\n",
    "+--------------------+--------------------+-------------------+--------------------+--------------------+\n",
    "|[SFO,San Francisc...|[1010700,0,SFO,BO...|[BOS,Boston,MA,USA]|[1010635,-6,BOS,B...|[BUF,Buffalo,NY,USA]|\n",
    "|[SFO,San Francisc...|[1010700,0,SFO,BO...|[BOS,Boston,MA,USA]|[1011059,13,BOS,B...|[BUF,Buffalo,NY,USA]|\n",
    "|[SFO,San Francisc...|[1010700,0,SFO,BO...|[BOS,Boston,MA,USA]|[1011427,19,BOS,B...|[BUF,Buffalo,NY,USA]|\n",
    "|[SFO,San Francisc...|[1010700,0,SFO,BO...|[BOS,Boston,MA,USA]|[1020635,-4,BOS,B...|[BUF,Buffalo,NY,USA]|\n",
    "|[SFO,San Francisc...|[1010700,0,SFO,BO...|[BOS,Boston,MA,USA]|[1021059,0,BOS,BU...|[BUF,Buffalo,NY,USA]|\n",
    "+--------------------+--------------------+-------------------+--------------------+--------------------+\n",
    "only showing top 5 rows\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you have your list of airports, determine the 10 most popular layover airports between SFO and BUF. Expected output:\n",
    "```\n",
    "+---+---------------+-------+\n",
    "| id|           City|  count|\n",
    "+---+---------------+-------+\n",
    "|JFK|       New York|1233728|\n",
    "|ORD|        Chicago|1088283|\n",
    "|ATL|        Atlanta| 285383|\n",
    "|LAS|      Las Vegas| 275091|\n",
    "|BOS|         Boston| 238576|\n",
    "|CLT|      Charlotte| 143444|\n",
    "|PHX|        Phoenix| 104580|\n",
    "|FLL|Fort Lauderdale|  96317|\n",
    "|EWR|         Newark|  95370|\n",
    "|MCO|        Orlando|  88615|\n",
    "+---+---------------+-------+\n",
    "only showing top 10 rows\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
