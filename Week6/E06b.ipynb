{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## E-Learning 6b - Using GraphFrames\n",
    "GraphFrames provide some common graph algorithms you can use (they are pretty established and you can find out more about them in Wikipedia):\n",
    "\n",
    "| Algorithm                     | Use Cases                               |\n",
    "|:------------------------------| :---------------------------------------|\n",
    "| PageRank                      | Finds important vertices                |\n",
    "| Connected Components          | Finds groups of components, communities |\n",
    "| Strongly Connected Components | Finds groups of components, communities |\n",
    "| Label Propagation             | Finds groups of components, communities |\n",
    "| Shortest Path                 | Finds path between a set of vertices    |\n",
    "| Breadth-First Search          | Finds path between a set of vertices    |\n",
    "| Triangle Counting             | Find subgraphs                          |\n",
    "\n",
    "\n",
    "In this exercise, you will learn how to do more interesting things with graphs, in particular, finding the most cited paper as well as the most influential paper in the High Energy Physics Theory citation graph network. We will be using the file `cit-HepTH.txt` in the `data` directory. The file contains the paper citation network of the Arxiv High Energy Physics Theory category. There are 27770 nodes and 352807 edges in this network. The start of the file looks like this:\n",
    "```\n",
    "1001  9304045\n",
    "1001  9308122\n",
    "1001  9309097\n",
    "1001  9311042\n",
    "1001  9401139\n",
    "1001  9404151\n",
    "1001  9407087\n",
    "1001  9408099\n",
    "```\n",
    "Each data line represents one edge of the graph, with the vertex IDs of the source and destination vertices. In this case, each vertex ID refers to a particular physics paper. In the context of a paper citation, the source vertex is the newer paper, and the destination vertex is the older paper being cited by the newer paper. For example, paper 1001 cites paper 9304045. It also cites a bunch of other papers too.\n",
    "\n",
    "First, we need to create the schema and then load the data into a DataFrame:\n",
    "```python\n",
    "from pyspark.sql.types import *\n",
    "\n",
    "edge_schema = StructType( [\n",
    "    StructField('src', StringType(), False),\n",
    "    StructField('dst', StringType(), False)] )\n",
    "\n",
    "edges = spark.read.csv(\"/home/training/data/cit-HepTh.txt\", header=False, sep=\"\\t\", schema=edge_schema)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Second, we need to get the set of unique paper ids from the edges DataFrame to create our vertices DataFrame. A paper id may appear only in the first or second column or may appear in both. We can extract the first and second column separately, union them and use distinct to remove duplicate paper ids:\n",
    "```python\n",
    "v = edges.select(\"src\").union(edges.select(\"dst\")).distinct()\n",
    "vertices = v.withColumnRenamed(\"src\", \"id\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the vertices and edges DataFrames, we can now construct our graph:\n",
    "```python\n",
    "from graphframes import *\n",
    "graph = GraphFrame(vertices, edges)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let us find out the id of the theoretical physics paper that is most frequently cited. The **indegree** of a vertex of a graph is the number of edges going into that vertex. In our context, a vertex representing paper *p* has an edge going into another vertex representing paper *q* if paper *p* cites paper *q*. In other words, the number of edges going into the vertex representing paper *q* (its indegree) gives us the number of papers that cite paper *q*. Therefore, the vertex with the highest indegree represents the most cited paper.\n",
    "\n",
    "The GraphFrames API provides an `inDegrees` method that returns the indegree of each vertex in the graph as a DataFrame with two columns:\n",
    "- “id”: the ID of the vertex\n",
    "- “inDegree”: the indegree of the vertex\n",
    "\n",
    "From this DataFrame, we just need to sort the inDegree in descending order and the first entry would give us the id of the most cited paper. Here is how we can do it:\n",
    "```python\n",
    "indeg = graph.inDegrees\n",
    "indeg.orderBy(indeg.inDegree.desc()).show(1)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The previous task has not taken full advantage of the power of graphs. Let us make use of one of the best-known algorithms in graph processing - **PageRank**. Although Larry Page of Google invented the `PageRank` algorithm (hence the name) to rank webpages on the World Wide Web, the algorithm can be used to measure the influence of vertices in any graph. We shall apply the PageRank algorithm to find the most influential paper in our theoretical physics citation network as follows (may take more than 30 minutes to complete, run it only if you can afford to wait):\n",
    "```python\n",
    "from pyspark.sql.functions import *\n",
    "graph.cache()\n",
    "ranks = graph.pageRank(tol=0.01)\n",
    "ranks.vertices.orderBy(desc(\"pagerank\")).select(\"id\", \"pagerank\").show(10)\n",
    "```\n",
    "The value 0.01 passed to the `pageRank` method is called the tolerance (`tol`), a parameter that sets the tradeoff between speed and accuracy of the final result. If we set it too high, we will stop the algorithm too early and our results would not be that accurate; too low, and the algorithm will continue on for too long without adding anything to the accuracy of the results.\n",
    "\n",
    "The `pageRank` method will return a new GraphFrame with new vertices column `pagerank` and new edges column `weight`. For example:\n",
    "```python\n",
    "ranks.vertices.show(5)\n",
    "ranks.edges.show(5)\n",
    "```\n",
    "would show the following:\n",
    "```\n",
    "+-------+-------------------+\n",
    "|     id|           pagerank|\n",
    "+-------+-------------------+\n",
    "|9504090|  26.77700565284537|\n",
    "|9612108| 0.6134551415423843|\n",
    "|9912102|0.25215966125200706|\n",
    "|   1119|             0.1755|\n",
    "|9903104| 0.2077752101929372|\n",
    "+-------+-------------------+\n",
    "only showing top 5 rows\n",
    "\n",
    "+-------+-------+--------------------+\n",
    "|    src|    dst|              weight|\n",
    "+-------+-------+--------------------+\n",
    "|9712028|9605150|0.038461538461538464|\n",
    "|9909030|9601078|              0.0625|\n",
    "|9909030|9905038|              0.0625|\n",
    "|9711200|9704112|0.018518518518518517|\n",
    "|9912242|9907209| 0.14285714285714285|\n",
    "+-------+-------+--------------------+\n",
    "only showing top 5 rows\n",
    "```\n",
    "Now, by sorting on the `pagerank` column of the vertices DataFrame in `ranks`, we would be able to find the most influential paper according to the `PageRank` algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
