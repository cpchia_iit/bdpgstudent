{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## E-Learning 6a - Using GraphFrames\n",
    "A graph is a structure to represent entities that are connected through relationships. It consists of vertices (or nodes) and the edges (or arcs, or links) that connect them. A computer network can be represented as a graph where the nodes represent computing devices with an IP address while the edges represent the communication links between the devices. We can also represent a social network using a graph where the nodes represent people while the edges represent the relationship between people e.g. friends, siblings etc.\n",
    "\n",
    "GraphFrames utilizes the power of Apache Spark DataFrames to support general graph processing. In Spark, edges are directed (they have a source and destination vertex), and both edges and vertices have property objects attached to them. The graph is permitted to have multiple edges between any pair of vertices. Moreover, each edge is directed and defines a unidirectional relationship. For example, in a Twitter graph, a user can follow another but the converse does not need to be true. To model bidirectional links, such as a Facebook friendship, we need to define two edges between the nodes, and these edges should point in opposite directions. \n",
    "\n",
    "Once represented as a graph, some problems can be easily solved using graph algorithms. In this exercise, we will construct a tiny social network using GraphFrames. You will be using the data files `people.csv` and `links.csv` from the data directory. Each line in the `people.csv` file represents information about a person. The information consists of an id, name and age. For example:\n",
    "```\n",
    "1,Alice,20\n",
    "2,Bob,18\n",
    "3,Charlie,30\n",
    "4,Dave,25\n",
    "5,Eve,30\n",
    "6,Faith,21\n",
    "7,George,34\n",
    "8,Harvey,47\n",
    "9,Ivy,21\n",
    "```\n",
    "Each line in the `links.csv` file represents information about the relationship between two persons. For example,\n",
    "```\n",
    "1,2,friend\n",
    "1,3,sister\n",
    "3,2,boss\n",
    "2,4,brother\n",
    "4,5,client\n",
    "6,7,cousin\n",
    "7,9,coworker\n",
    "8,9,father\n",
    "1,9,friend\n",
    "```\n",
    "Here, \"1, 3, sister\" indicates that person with id 3 is the sister of person with id 1.\n",
    "\n",
    "First, create the schema for the datasets so that we can load the datasets into DataFrames:\n",
    "```python\n",
    "from pyspark.sql.types import *\n",
    "\n",
    "vertex_schema = StructType( [\n",
    "    StructField('id', IntegerType(), False),\n",
    "    StructField('name', StringType(), False),\n",
    "    StructField('age', IntegerType(), False) ] )\n",
    "\n",
    "edge_schema = StructType( [\n",
    "    StructField('src', IntegerType(), False),\n",
    "    StructField('dst', IntegerType(), False),\n",
    "    StructField('relation', StringType(), False) ] )\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "GraphFrames are constructed from two basic constructs:\n",
    "- **Vertices DataFrame** where each row is treated as a vertex. The frame must have a mandatory id column for the ID of the vertex.\n",
    "- **Edges DataFrame** where each row represents an edge. It must have at least two columns src and dst, which refer to the vertices DataFrame, and defines the connection between two vertices.\n",
    "\n",
    "We shall now create these two DataFrames from our datasets:\n",
    "```python\n",
    "vertices = spark.read.csv(\"/home/training/data/people.csv\", header=False, schema=vertex_schema)\n",
    "edges = spark.read.csv(\"/home/training/data/links.csv\", header=False, schema=edge_schema)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we shall create our social graph which is called `tinySocial`:\n",
    "```python\n",
    "from graphframes import *\n",
    "tinySocial = GraphFrame(vertices, edges)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us display all the vertices and edges:\n",
    "```python\n",
    "tinySocial.vertices.show()\n",
    "tinySocial.edges.show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have a GraphFrame, we can execute various queries on the graph. For example, suppose we want to print only the friend connections where a younger person \"points\" to an older person:\n",
    "```python\n",
    "friends = tinySocial.find(\"(a)-[e]->(b)\").filter(\"e.relation = 'friend'\").filter(\"a.age < b.age\")\n",
    "friends.show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What you have just did is known as **motif finding** which is finding structural patterns within a graph. The pattern `(a)-[e]->(b)` means to find all edges `e` from vertex `a` to vertex `b`. Note that a vertex is enclosed in parenthesis while an edge is enclosed in square brackets. Instead of `a`, `e` and `b`, you can use other names e.g. person1, action1 and person2. These names will be column names in the resulting DataFrame.\n",
    "\n",
    "A pattern can be expressed as a union of edges and edge patterns can be joined with semicolons. Within the pattern, you can specify an empty vertex () or an empty edge [] and you can additionally apply filters to the results of the `find` operation. For example, the following searches for two persons (`a` and `c`) who are indirectly connected through a third person `b` and `a` and `b` must be friends.\n",
    "```python\n",
    "motifs = tinySocial.find(\"(a)-[e1]->(b); (b)-[e2]-> (c)\").filter(\"e1.relation = 'friend'\")\n",
    "motifs.show()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results tell us that Alice is indirectly connected to Dave through Bob who is a friend of Alice."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
